import tensorflow as tf
# -------------------------------------------------------------------
# conv4d equivalent with dilation
# -------------------------------------------------------------------
import numpy as np

from tqdm import tqdm

import logging
import time
import os
from scipy.ndimage import measurements, interpolation
from scipy.io import savemat
import matplotlib.pyplot as plt
from ZSSR import ZSSR


iteration = 1


def zeroize_negligible_k(k, n):
    k_sorted = np.sort(k.flatten())

    k_n_min = 0.75 * k_sorted[-n - 1]

    filtered_k = np.clip(k - k_n_min, a_min=0, a_max=100)

    return filtered_k / (filtered_k.sum() + 1e-8)


def kernel_shift(kernel, sf):
    base_kernel = kernel[0, :, :, :, :, 0]
    current_center_of_mass = measurements.center_of_mass(base_kernel)

    wanted_center_of_mass = np.array(base_kernel.shape) // 2 + 0.5 * (np.array(sf) - (np.array(base_kernel.shape)) % 2)

    shift_vec = wanted_center_of_mass - current_center_of_mass

    np.nan_to_num(shift_vec, copy=False, nan=0)

    base_kernel = np.pad(base_kernel, np.int(np.ceil(np.max(np.abs(shift_vec)))) + 1, 'constant')

    base_kernel = interpolation.shift(base_kernel, shift_vec)

    return np.expand_dims(base_kernel, axis=[0, -1])


def post_process_k(k, n):
    significant_k = zeroize_negligible_k(k, n)

    centralized_k = kernel_shift(significant_k, sf=2)

    return centralized_k


def save_final_kernel(kernel, conf, k_4=None):
    savemat(
        os.path.join(conf.output_dir_path, "%s_kernel_x2.math" % conf.img_name), {'Kernel': kernel}
    )

    if conf.x4:
        savemat(
            os.path.join(conf.output_dir_path, "%s_kernel_x4.mat" % conf.img_name), {'Kernel': k_4}
        )


def run_zssr(k_2, conf):
    if conf.do_ZSSR:
        start_time = time.time()
        print('~'*30 + '\nRunning ZSSR X%d...' % (4 if conf.X4 else 2))
        if conf.X4:
            kernels = [k_2, analytic_kernel(k_2)]
            print(kernels[0].shape)
            print(kernels[1].shape)
            sr1 = ZSSR(conf.input_image_path, scale_factor=2, kernels=[k_2], is_real_img=conf.real_image, noise_scale=conf.noise_scale).run()
            sr = ZSSR(conf.input_image_path, scale_factor=4, kernels=[analytic_kernel(k_2)],
                 is_real_img=conf.real_image, noise_scale=conf.noise_scale).run()
        else:
            sr = ZSSR(conf.input_image_path, scale_factor=2, kernels=[k_2], is_real_img=conf.real_image, noise_scale=conf.noise_scale).run()
        max_val = 255 if sr.dtype=='uint8' else 1
        plt.imsave(os.path.join(conf.output_dir_path, 'ZSSR_%s.png' % conf.img_name), sr, vmin=0, vmax=max_val, dpi=1)
        runtime = int(time.time() - start_time)
        print('Completed! runtime=%d:%d\n' % (runtime // 60, runtime % 60) + '~' * 30)


def analytic_kernel(kernel):
    k_size1 = kernel.shape[1]
    k_size2 = kernel.shape[3]

    big_k = np.zeros(shape=(1,3*k_size1-2, 3*k_size1-2, 3*k_size2-2, 3*k_size2-2, 1))

    for r in range(k_size1):
        for c in range(k_size1):
            for j in range(k_size2):
                for k in range(k_size2):
                    big_k[0:1,2*r:2*r+k_size1, 2*c:2*c+k_size1, 2*j: 2*j + k_size2, 2*k:2*k+k_size2,0:1] += kernel[0,r, c, j, k,0] * kernel

    crop1 = k_size1 // 2
    crop2 = k_size2 // 2

    cropped_big_k = big_k[:,crop1: -crop1, crop1: -crop1, crop2:-crop2, crop2: -crop2,:]

    return cropped_big_k / cropped_big_k.sum()


def spectral_norm(input_tensor, name, graph=None):
    # https://stackoverflow.com/a/57529955/16630494
    w_shape = input_tensor.shape.as_list()
    w = tf.reshape(input_tensor, [-1, w_shape[-1]])

    with graph.as_default():
        with tf.variable_scope('u' + name, reuse=tf.AUTO_REUSE):
            u = tf.get_variable('u' + name, [1, w_shape[-1]], initializer=tf.random_normal_initializer(), trainable=False)

    u_hat = u
    v_hat = None

    for i in range(iteration):
        with graph.as_default():
            v_ = tf.matmul(u_hat, tf.transpose(w))
            v_hat = tf.nn.l2_normalize(v_)

            u_ = tf.matmul(v_hat, w)
            u_hat = tf.nn.l2_normalize(u_)

    with graph.as_default():
        u_hat = tf.stop_gradient(u_hat)
        v_hat = tf.stop_gradient(v_hat)

        sigma = tf.matmul(tf.matmul(v_hat, w), tf.transpose(u_hat))

        with tf.control_dependencies([u.assign(u_hat)]):
            w_norm = w / (sigma + 1e-8)
            w_norm = tf.reshape(w_norm, w_shape)

    return w_norm


if __name__ == '__main__':
    batch = 1
    x_dim = 5
    y_dim = 5
    z_dim = 5
    t_dim = 64
    in_channels = 3
    out_channels = 10

    a = tf.zeros(shape=
    (
        batch,
        x_dim,
        y_dim,
        z_dim,
        t_dim,
        in_channels
    )
    )
    k = tf.zeros(shape=
    (
        x_dim,
        y_dim,
        z_dim,
        t_dim,
        # out_channels,
        in_channels,
        out_channels
    )
    )

    k1 = tf.zeros(shape=
    (
        x_dim,
        y_dim,
        z_dim,
        t_dim,
        in_channels,
        out_channels
    )
    )

    # o = convolve4d_transpose(a, k, padding='SAME')
    # o = convolve4d(a, k, padding='SAME')

    # print(o.shape)

    from easydict import EasyDict
    conf = EasyDict()
    conf.output_dir_path = './'
    conf.img_name = 'test_img'
    conf.x4 = True
    conf.do_ZSSR = True
    conf.X4 = True
    conf.input_image_path = '/Users/tanishabhayani/Desktop/pisa.png'
    conf.real_image = '/Users/tanishabhayani/Desktop/pisa.png'
    conf.noise_scale = 1

    final_kernel = post_process_k(np.random.random(size=(1,1,1,4,4,1)), 1)
    print(final_kernel.shape)
    save_final_kernel(final_kernel, conf)
    run_zssr(final_kernel, conf)

    # print(k.shape.rank)
    #
    # o = convolve3d_transpose(a, k, padding='VALID', strides=[1,1,1,])
    #
    # print(o.shape, 'conv2d transpose')
    #
    # strides = [1,1,1,1,]
    #
    # output_shape = [(a.shape[i + 1] - 1) * strides[i] + k.shape[i] for i in
    #                     range(2)]
    # output_shape.append(k.shape[-2])
    # output_shape.insert(0, a.shape[0])
    #
    # print('main', output_shape)
    # print('now = ', output_shape)
    # if padding == 'SAME':
    # output_shape = a.shape.as_list()
    # print(output_shape)
    # output_shape[-1] = k.shape[-2]
    #
    # print(tf.nn.conv2d_transpose(a, k, output_shape=output_shape, strides=[1,1,1,1], padding='VALID').shape)

# test_conv3d()
