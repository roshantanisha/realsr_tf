from convolution4d import *
from RRDBNet_tf import *
from MSRResNet_tf import *
from discriminator_vgg_arch_tf import *


def prelu(_x, graph):
    with graph.as_default():
        alphas = tf.get_variable('alpha', _x.get_shape()[-1],
                                 initializer=tf.constant_initializer(0.0),
                                 dtype=tf.float32)
        pos = tf.nn.relu(_x)
        neg = alphas * (_x - abs(_x)) * 0.5

        return pos + neg


def residual_block(x, channels, graph):
    with graph.as_default():
        conv1 = conv4d(
            x=x,
            in_channels=channels,
            out_channels=channels,
            kernel_size_1=3,
            kernel_size_2=3,
            stride_1=1,
            stride_2=1,
            padding='SAME',
            bias=True
        )
        prelu1 = prelu(conv1, graph)
        conv2 = conv4d(
            x=prelu1,
            in_channels=channels,
            out_channels=channels,
            kernel_size_1=3,
            kernel_size_2=3,
            stride_1=1,
            stride_2=1,
            padding='SAME',
            bias=True
        )

        return x + conv2


class Generator:
    def __init__(self, n_res_blocks=8, conf=None):
        self.graph = tf.Graph()
        self.n_res_blocks = n_res_blocks
        self.conf = conf

    def build_model(self):
        with self.graph.as_default():
            self.input_placeholder = tf.placeholder(
                shape=(
                    self.conf.batch_size,
                    self.conf.x_dim,
                    self.conf.y_dim,
                    self.conf.h_dim,
                    self.conf.w_dim,
                    self.conf.in_channels
                ),
                dtype=tf.float32
            )

            block = list()

            block.append(
                conv4d(
                    x=self.input_placeholder,
                    in_channels=3,
                    out_channels=64,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True
                )
            )

            block.append(prelu(block[-1], self.graph))

            for i in range(self.n_res_blocks):
                block.append(
                    residual_block(
                        x=block[-1],
                        channels=64,
                        graph=self.graph
                    )
                )

            block_output = conv4d(
                x=block[-1],
                in_channels=64,
                out_channels=3,
                kernel_size_1=3,
                kernel_size_2=3,
                stride_1=1,
                stride_2=1,
                padding='SAME',
                bias=True
            )

            self.output_layer = block_output

            self.final_output = tf.clip_by_value(self.input_placeholder + block_output, 0, 1)


def define_G(opt):
    opt_net = opt['network_G']
    print(opt_net)
    which_model = opt_net['which_model_G']

    if which_model == 'MSRResNet':
        netG = MSRResNet(
            in_nc=opt_net['in_nc'],
            out_nc=opt_net['out_nc'],
            nf=opt_net['nf'],
            nb=opt_net['nb'],
            upscale=opt_net['scale']
        )
        netG.build_model(opt_net)
    elif which_model == 'RRDBNet':
        netG = RRDBNet(
            in_nc=opt_net['in_nc'],
            out_nc=opt_net['out_nc'],
            nf=opt_net['nf'],
            nb=opt_net['nb']
        )
        netG.build_model(opt_net)
    else:
        raise NotImplementedError('Generator model not recognized')

    return netG


def define_D(opt):
    opt_net = opt['network_D']
    which_model = opt_net['which_model_D']

    if which_model == 'discriminator_vgg_128':
        netD = Discriminator_VGG_128(opt_net)
        print(netD.graph)
    elif which_model == 'discriminator_vgg_256':
        netD = Discriminator_VGG_256(opt_net)
    elif which_model == 'discriminator_vgg_512':
        netD = Discriminator_VGG_512(opt_net)
    elif which_model == 'NLayerDiscriminator':
        netD = NLayerDiscriminator(
            input_nc=opt_net['in_nc'],
            ndf=opt_net['nf'],
            n_layers=opt_net['nLayer'],
            conf=opt_net
        )
    return netD

