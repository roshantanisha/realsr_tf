import cv2
import math
import numpy as np
import matplotlib.pyplot as plt
import lmdb
import pandas as pd
import os
import zlib
from scipy.ndimage import convolve
from imresize import imresize
from PIL import Image
from scipy.ndimage import gaussian_filter
from scipy.signal import convolve2d


H = 128
W = 128
H1 = 376
W1 = 541
X = 14
Y = 14


def get_lf_array(raw_array, C):
    S = 14
    T = 14
    H = 376
    W = 541
    # C = 3
    print(raw_array.shape)
    lf = np.zeros(shape=(S, T, H, W, C), dtype=np.uint8)
    for x in range(S):
        for y in range(T):
            temp = raw_array[x::S, y::T, :]
            # print(temp.shape, temp[0, 0, ...].shape)
            if len(raw_array.shape) == 3:
                lf[x, y, :temp.shape[0], :temp.shape[1], :] = temp
            else:
                lf[x, y, :temp.shape[2], :temp.shape[3], :] = temp[0, 0, ...]

    # print(lf.shape)
    return lf


def patch(image_array, patch_size, train=False, sample_patches=None, angular_patch_size=5):
    channels = image_array.shape[-1]
    print('channels = ', channels)
    image_array = get_lf_array(image_array, channels)
    new_image = image_array.reshape((-1, H1, W1, channels))
    # print(new_image.shape)
    H = patch_size
    W = patch_size
    H_prime = math.ceil(H1/H)*H
    W_prime = math.ceil(W1/W)*W

    # print(H_prime, W_prime)

    new_image_prime = np.zeros(shape=(new_image.shape[0], H_prime, W_prime, channels), dtype=np.uint8)
    # print(new_image_prime.shape)
    new_image_prime[:, :H1, :W1, :] = new_image

    # print('actual number of patches = ', new_image.shape[0]*(H_prime//H)*(W_prime//W))
    # patches = np.zeros(shape=(new_image.shape[0]*(H_prime//H)*(W_prime//W), H, W, 3), dtype=np.uint8)
    patches = []
    # print(patches.shape)
    H_multiplier = H_prime//H
    W_multiplier = W_prime//W

    i = 0
    for each_image in new_image_prime:
        for j in range(H_multiplier):
            for k in range(W_multiplier):
                temp = each_image[j*H:H*(j+1), (k)*W:(k+1)*W]
                # if np.all(temp == 0) and train:
                #     continue
                # patches[i] = each_image[j*H:H*(j+1), (k)*W:(k+1)*W]
                patches.append(temp)
                i = i + 1

    patches = np.array(patches)
    # if train:
    print(patches.shape)
    patches = patches.reshape((-1, patch_size, patch_size, 14, 14, channels))
    # if sample_patches is not None:
    #     patches = patches[np.random.choice(np.arange(patches.shape[0]), sample_patches, replace=True), ...]
    angular_patches = patch_angular(patches, angular_patch_size=angular_patch_size)
    if sample_patches is not None:
        angular_patches = angular_patches[np.random.choice(np.arange(patches.shape[0]), sample_patches, replace=True), ...]
    return patches, angular_patches


def load_patches(db, index, patch_size, train=False, sample_patches=None, angular_patch_size=5):
    img = load_image_from_db(db, index)
    patches = patch(img, patch_size, train, sample_patches, angular_patch_size=angular_patch_size)
    return img, patches


def get_gauss_filter(shape=(7, 7), sigma=1.2):
    """
    2D gaussian mask - should give the same result as MATLAB's
    fspecial('gaussian',[shape],[sigma]).

    :param shape: window size
    :param sigma: variance

    :return:      gaussian filter
    """
    m, n = [(ss - 1.) / 2. for ss in shape]
    y, x = np.ogrid[-m:m + 1, -n:n + 1]
    h = np.exp(-(x * x + y * y) / (2. * sigma * sigma))
    h[h < np.finfo(h.dtype).eps * h.max()] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h


def blur(hrlf, psf):
    """
    The blur method for light field imaging which generates
    the low-quality light field.

    :param hrlf: high-resolution light field
    :param psf:  blur kernel

    :return:     blurred light field
    """
    blurred_lfimgs = np.zeros_like(hrlf)
    ws = psf.shape[0]
    t = int((ws - 1) / 2)

    hrlf = np.concatenate([hrlf[:, :t, :], hrlf, hrlf[:, -t:, :]], axis=1)
    hrlf = np.concatenate([hrlf[:t, :, :], hrlf, hrlf[-t:, :, :]], axis=0)

    if hrlf.shape[2] == 3:
        blurred_lfimgs[:, :, 0] = convolve2d(hrlf[:, :, 0], psf, 'valid')
        blurred_lfimgs[:, :, 1] = convolve2d(hrlf[:, :, 1], psf, 'valid')
        blurred_lfimgs[:, :, 2] = convolve2d(hrlf[:, :, 2], psf, 'valid')
    else:
        blurred_lfimgs = convolve2d(np.squeeze(hrlf), psf, 'valid')
        blurred_lfimgs = np.expand_dims(blurred_lfimgs, axis=2)

    return blurred_lfimgs


def downsampling(data, rs=1, ra=1, nSig=1.2):
    '''
    Dowmsampling method on spatial or angular dimensions

    :param data: high-resolution light field (labels)
    :param rs:   spatial downsampling rate
    :param ra:   angular downsampling rate
    :param nSig: gaussian noise variation

    :return:     downsampled light field
    '''
    def spatial_downsampling(GT, rate=2):
        b, h, w, s, t, c = GT.shape
        psf = get_gauss_filter(shape=(7, 7), sigma=nSig)
        downsampled = np.zeros_like(GT)
        for n in range(b):
            for i in range(s):
                for j in range(t):
                    downsampled[n, :, :, i, j, :] = blur(GT[n, :, :, i, j, :], psf)
        downsampled = downsampled[:, ::rate, ::rate, :, :, :]
        return downsampled

    def angular_downsampling(GT, rate=2):
        downsampled = None
        if rate == 4:  # 2x2 -> 8x8
            GT = GT[:, :, :, :-1, :-1, :]
            downsampled = GT[:, :, :, 0:8:7, 0:8:7, :]
        elif rate == 3:  # 3x3 -> 9x9
            downsampled = GT[:, :, :, ::(rate+1), ::(rate+1), :]
        elif rate == 2:  # 5x5 -> 9x9
            downsampled = GT[:, :, :, ::rate, ::rate, :]
        elif rate == 0:  # 3x3 -> 7x7
            GT = GT[:, :, :, 1:-1, 1:-1, :]
            downsampled = GT[:, :, :, ::3, ::3, :]
        else:
            assert False, "Unsupported angular downsampling rate: {}.".format(rate)
        return GT, downsampled

    if rs != 1 and ra == 1:
        downsampled = spatial_downsampling(data, rate=rs)
        return downsampled
    elif ra != 1 and rs == 1:
        label, downsampled = angular_downsampling(data, rate=ra)
        return label, downsampled
    elif ra != 1 and rs != 1:
        label, downsampled = angular_downsampling(data, rate=ra)
        downsampled = spatial_downsampling(downsampled, rate=rs)

        return label, downsampled
    else:
        assert False, "Both spatial and angular downsampling rates are 1."


def load_image(image_file_path, patch_size, train=False, sample_patches=None, angular_patch_size=5):
    img = np.expand_dims(cv2.imread(image_file_path, 0), axis=-1)
    print('@@@ img.shape', img.shape)
    patches = patch(img, patch_size, train, sample_patches, angular_patch_size=angular_patch_size)
    return patches


def reduce_all_patches(patches, reduction_int):
    image = np.empty(shape=(patches.shape[0], patches.shape[1]//reduction_int, patches.shape[1]//reduction_int, 3))
    index = 0
    for each_patch in patches:
        image[index] = reduce_resolution(each_patch, reduction_int)

    return image


def reduce_resolution(each_image, reduction_int):
    new_image = cv2.resize(each_image, dsize=(each_image.shape[0]//reduction_int, each_image.shape[1]//reduction_int))
    return new_image


def patch_angular(lf_array, angular_patch_size=5):
    channels = lf_array.shape[-1]
    assert len(lf_array.shape) >= 5
    s = lf_array.shape[3]
    t = lf_array.shape[4]
    h = lf_array.shape[1]
    w = lf_array.shape[2]

    s_multiplier = math.ceil(s/angular_patch_size)
    t_multiplier = math.ceil(t/angular_patch_size)

    all_patches = []
    for ii in range(lf_array.shape[0]):

        num_patches = s_multiplier * t_multiplier

        new_lf_array = np.zeros(shape=(num_patches, h, w, angular_patch_size, angular_patch_size, channels))

        for i in range(s_multiplier):
            for j in range(t_multiplier):
                index = i*t_multiplier + j
                temp = lf_array[ii:ii+1, :, :, (i*angular_patch_size):(i+1)*angular_patch_size, j*angular_patch_size: (j+1)*angular_patch_size, ...]
                new_lf_array[index, :, :, :temp.shape[3], :temp.shape[4], :] = temp

        all_patches.append(new_lf_array)

    print('===>', np.concatenate(all_patches, axis=0).shape)
    return np.concatenate(all_patches, axis=0)


def restore_angular_patches(patches, scale):
    print('patches.shape = ', patches.shape)
    new_x = X * scale
    new_y = Y * scale

    x = patches.shape[1]
    y = patches.shape[2]

    num_patches = (patches.shape[0] * patches.shape[1] * patches.shape[2]) // (new_x * new_y) + 1
    new_img = np.zeros(shape=(num_patches, new_x, new_y, patches.shape[3], patches.shape[4], patches.shape[5]))
    print(new_img.shape)
    reshaped_patches = np.reshape(patches, (-1, patches.shape[3], patches.shape[4], patches.shape[5]))

    ii = 0
    index = 0
    while index < reshaped_patches.shape[0]:
        for i in range(0, new_x, x):
            for j in range(0, new_y, y):
                xi = min(i+x, new_x)
                yi = min(j+y, new_y)
                patch_size = min(ii + (xi - i) * (yi - j), reshaped_patches.shape[0])
                size = patch_size - ii
                if size < 0:
                    break
                new_img[index:index+1, i:i+(size // (yi - j)), j:yi, ...] = reshaped_patches[ii:patch_size].reshape((1, size // (yi-j), (yi-j), patches.shape[3], patches.shape[4], patches.shape[5]))
                ii = ii + (xi - i)*(yi - j)
                if ii > reshaped_patches.shape[0]:
                    break
        if patch_size < 0:
            break
        index += 1

    return new_img


def increase_resolution(each_image, increase_int):
    new_image = cv2.resize(each_image, dsize=(each_image.shape[0]*increase_int, each_image.shapae[1]*increase_int))
    return new_image


def restore_patches(patches, img_shape, scale=1):
    if len(img_shape) == 3:
        realH, realW, _ = img_shape
    if len(img_shape) == 5:
        H = img_shape[0]
        W = img_shape[1]
    elif len(img_shape) == 6:
        new_img = restore_angular_patches(patches, 2)
        img1 = np.swapaxes(np.swapaxes(new_img, 1, 3), 2, 4)
        img_shape = img1.shape
        H = img_shape[1]
        W = img_shape[2]
        patches = img1
    print(patches.shape)
    # _, H, W, _ = patches.shape
    H_prime = math.ceil(H1*scale/H)*H
    W_prime = math.ceil(W1*scale/W)*W
    H_multiplier = H_prime // H
    W_multiplier = W_prime // W
    patches_size = img_shape[3]*img_shape[4]*img_shape[0]
    patches1 = np.reshape(patches, (-1, H, W, img_shape[5]))
    image = np.zeros(shape=(patches_size, H_prime, W_prime, 3))
    print(image.shape)
    i = 0
    index = 0
    while index < patches1.shape[0]:
        for j in range(H_multiplier):
            for k in range(W_multiplier):
                image[i, j*H:(j+1)*H, (k)*W:(k+1)*W] = patches1[index]
                index = index + 1
        i += 1

    lf_image = image[:, :H1*scale, :W1*scale].reshape((X*scale, Y*scale, H1*scale, W1*scale, 3))
    if len(img_shape) == 3:
        actual_image = np.zeros(shape=(realH + realH % (H1*scale), realW + realW % (W1*scale), 3))
        S = 14
        T = 14
        for x in range(S):
            for y in range(T):
                temp = lf_image[x, y, :(H1*scale), :(W1*scale), :]
                actual_image[x::S, y::T, :][:temp.shape[0], :temp.shape[1], :] = temp

        return actual_image[:realH, :realW, :]
    else:
        return lf_image


def load_data_db(db_path):
    db = lmdb.open(db_path)
    file = pd.read_csv(os.path.dirname(db_path) +'/' + db_path.split('/')[-1].split('.')[0] + '.csv').values.tolist()
    return db, file


def load_image_from_db(db, index):
    with db.begin() as txn:
        image_bytes = txn.get('name_{}'.format(index).encode())
        img = np.frombuffer(zlib.decompress(image_bytes), dtype=np.uint8).reshape((14, 14, H1, W1, 3))

    return img


def load_train_test_validation_files(data_file_path, load_db=False):
    train = pd.read_csv(data_file_path + '/train.csv')
    test = pd.read_csv(data_file_path + '/test.csv')
    validation = pd.read_csv(data_file_path + '/validation.csv')

    if load_db:
        train_db = lmdb.open(data_file_path + '/train.db')
        test_db = lmdb.open(data_file_path + '/test.db')
        validation_db = lmdb.open(data_file_path + '/validation.db')

        return (train, train_db), (validation, validation_db), (test, test_db)

    return (train, None), (validation, None), (test, None)


def convert_rgb_to_gray(img):
    new_image = np.zeros(shape=img.shape[0:-1])
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            new_image[i, j] = cv2.cvtColor(img[i, j, ...].astype('uint8'), cv2.COLOR_RGB2GRAY)

    return np.expand_dims(new_image, axis=-1)


