# Objective (Part 1):
Use GAN (Generative Adversarial Network) framework: The aim here is to learn the correlations between LR and HR 4D volume of images using the following Loss functions: pixel loss, perceptual loss, and adversarial loss. 


# Introduction:

The generator adopts RRDB structure, and the resolution of the generated image will be enlarged for 4 times. Several losses are applied to training includes pixel loss, perceptual loss, and adversarial loss. The pixel loss L1 uses L1 distance. Perceptual loss Lper uses the inactive features of VGG-19 [33], which helps to enhance the visual effect of low-frequency features such as edges. Adversarial loss Ladv is used to enhance the texture details of the generated image to make it look more realistic.  Different from default ESRGAN setting, patch discriminator (NLayer) is used instead of VGG-128 because of two conveniences: 1) VGG-128 limits the size of the generated image to 128, making multi-scale training inconvenient. 2) VGG-128 contains a deeper network and its fixed fully connected layers make the discriminator pay more attention to global features and ignore local features. In contrast, a patch discriminator with fully convolution structure, which has a fixed receptive field is used. For example, a three-layer network corresponds to a 70×70 patch. That is, each output value by the discriminator is only related to the patch of local fixed area. The patch losses will be fed back to the generator to optimize the gradient of local details. Note that the final error is the average of all local errors to guarantee global consistency.

# Full Network Architecture:
**1. Input to the Network**: Downsampled Images

**2. Output of the Network**: Upscaled images.

**3. How many layers?**: 
 * RRDBNet Generator: 8 Convolution based layers + `nb` number of RRDB blocks
 * MSRNet Generator: 6 Convolution Layers and nb number of residual blocks without batch norm
 * Discriminator VGG128:  5 Convolution based blocks and 1 Fully Connected based block.
 * Discriminator VGG256: 6 Convolutoin based blocks and 1 Fully Connected Based block
 * Discriminator VGG512: 7 Convolution based blocks and 1 Fully Connected Based block
 * Discriminator NLayer: n_layers + 1 convolution + BN based blocks wrapped with convolution blocks

**4. What does each layer consist of (conv, transposed conv, ReLU, Batch Normalization, weight layer, etc.)? and how many of each?**
 * RRDBNet Generator Feature Block: Each RRDB block contains 3 ResidualDenseBlock. Each ResidualDenseBlock contains 5 Convolution Layers whose each output is then fused with the input for further layer processing
 * MSRNet Residual Block:  contains 2 convolution layers
 * Discriminator VGG128:  5 Convolution based blocks contains (Conv -> Batch Norm -> LeakyReLU) X 2 and 1 Fully Connected based block which contains 2 flly connected layers one with 100 hidden units and other with 1 output unit. 
* Discriminator VGG256: 1 Convolution block consisting of Conv -> LeakyReLU -> Conv -> BatchNorm -> LeakyReLU and 5 Convolution based blocks contains (Conv -> Batch Norm -> LeakyReLU) X 2 and 1 Fully Connected based block which contains 2 flly connected layers one with 100 hidden units and other with 1 output unit.
* Discriminator VGG512: 1 Convolution block consisting of Conv -> LeakyReLU -> Conv -> BatchNorm -> LeakyReLU and 6 Convolution based blocks contains (Conv -> Batch Norm -> LeakyReLU) X 2 and 1 Fully Connected based block which contains 2 flly connected layers one with 100 hidden units and other with 1 output unit.
 * Discriminator NLayer: n_layers blocks contains Conv -> BatchNorm -> LeakyReLU
 

**5. How are these layers connected? (You can show them by drawing/figure if it’s hard to explain).**

<h2><p align="center">
n_layer Discriminator</p></h2>

![n_layer Discriminator Graph](nlayer_discriminator.png)

<h2><p align="center">
VGG128 Discriminator</p></h2>

![vgg 128 Discriminator Graph](discriminator_vgg128.png)

<h2><p align="center">
VGG256 Discriminator</p></h2>

![vgg 256 Discriminator Graph](discriminator_vgg256.png)

<h2><p align="center">
VGG512 Discriminator</p></h2>

![vgg 512 Discriminator Graph](discriminator_vgg512.png)


<h2><p align="center">
Generator MSRResNet Graph</p></h2>

![Generator MSRResNet Graph](generator_msrresnet.png)

<h2><p align="center">
Generator RRDBNet Graph</p></h2>

![Generator RRDBNet Graph](rrdbnet0.png)
![Generator RRDBNet Graph](rrdbnet1.png)
![Generator RRDBNet Graph](rrdbnet2.png)
![Generator RRDBNet Graph](rrdbnet3.png)


**6. How may filters?** 
 * RRDBNet Generator:  15*nb + 8
 * MSRNet Generator: 2*nb + 7
 * Discriminator VGG128: 10
 * Discriminator VGG256: 12
 * Discriminator VGG512: 14
 * Discriminator NLayer: n_layers + 2

**7. Filter sizes?**

[(kernel_size, kernel_size)Angular (kernel_size, kernel_size)Spatial out_channels] X number_of_times_repeated
 * RRDBNet Generator:  
   * [(3,3)A (3,3)S gc] X 4 X 3 X nb
   * [(3,3)A (3,3)S nf] X 1 X 3 X nb + 5
   * [(1,1)A (1,1)S nf*(2^4)] X 2
   * [(3,3)A (3,3)S 3] X 1
 * MSRNet Generator: 
   * [(3,3)A (3,3)S nf] X 2 X nb + 2
   * [(3,3)A (3,3)S nf * (2^4)] X 1
   * [(3,3)A (3,3)S nf * (3^4)] X 1
   * [(3,3)A (3,3)S nf * (4^4)] X 2
   * [(3,3)A (3,3)S 3] X 1
 * Discriminator VGG128: 
   * [(3,3)A (3,3)S nf] X 1
   * [(4,4)A (4,4)S nf] X 1
   * [(3,3)A (3,3)S nf*2] X 1
   * [(4,4)A (4,4)S nf*2] X 1
   * [(3,3)A (3,3)S nf*4] X 1
   * [(4,4)A (4,4)S nf*4] X 1
   * [(3,3)A (3,3)S nf*8] X 2
   * [(4,4)A (4,4)S nf*8] X 2
 * Discriminator VGG256:
   * [(3,3)A (3,3)S nf] X 1
   * [(4,4)A (4,4)S nf] X 1
   * [(3,3)A (3,3)S nf*2] X 1
   * [(4,4)A (4,4)S nf*2] X 1
   * [(3,3)A (3,3)S nf*4] X 1
   * [(4,4)A (4,4)S nf*4] X 1
   * [(3,3)A (3,3)S nf*8] X 3
   * [(4,4)A (4,4)S nf*8] X 3
 * Discriminator VGG512:
   * [(3,3)A (3,3)S nf] X 1
   * [(4,4)A (4,4)S nf] X 1
   * [(3,3)A (3,3)S nf*2] X 1
   * [(4,4)A (4,4)S nf*2] X 1
   * [(3,3)A (3,3)S nf*4] X 1
   * [(4,4)A (4,4)S nf*4] X 1
   * [(3,3)A (3,3)S nf*8] X 4
   * [(4,4)A (4,4)S nf*8] X 4
 * Discriminator NLayer:
    * [(4,4)A (4,4)S ndf] X 1
    * [(4,4)A (4,4)S ndf*2] X 1
    * [(4,4)A (4,4)S ndf*4] X 1 -> repeated for n_layers
    * [(4,4)A (4,4)S 1] X 1

**8. What is the batch size?** 1

**9. Does it have a patch discriminator?** Yes

**10. What loss functions? How is it calculated?**
  * Generator:
    * Generative Loss: Sigmoid Cross entropy, discriminator output for generated and actual image
    * Perceptual Loss: The VGG Features output for the actual image and generated image, the loss considered here is either absolute difference or mean squared error to check the difference between the features of VGG19.  
    * Pixel Loss: Charbonnier Loss (L1) between the Generative model output and actual output that should be.
    * total loss - sum of above losses
  * Discriminator: 
    * Generative Loss - Mean Absolute Error between the actual labels, and predicted ones.


# How is the training performed?

**1. Did you crop the images?** Yes, cropped to patches to (6, 6, 96, 96) specifically for VGG19

**2. Any data augmentation?** No

**3. Number of epochs?** 1

**4. Training parameters: momentum parameter? Learning rate initialization? Decay after how many epochs? Decay factor?** MultiStepLR is used.

**5. Loss function over all? Or different for each update?** We check total gan loss and discriminator loss and not individual losses.

**6. Evaluation metrics?** Total Gan Loss and Discriminator loss.


# Third-party code:

**1. What did you keep the same? Or how is it similar to each code?** We converted the Conv -> Conv4D and torch code of the RealSR to the tensorflow code, here the code are similar to each other in all aspects.

**2. What modifications did you do in details please?** Almost everything is implemented from the torch code. No extra modification is added.   

**3. Why did you do these modifications? Like what is the purpose of the modifications?** To make it work for 4D images.



# System Specification
* OS: MacOS Catalina version: 10.15.6
* Python Version: Python3.7 (miniconda environment)
* Deep Learning Framework: Tensorflow version 1.14
* Dependencies
  * `tensorflow==1.15`
  * `tqdm==4.62.0`
  * `opencv-python==4.5.3.56`
  * `unrar==0.4` 
  * `scipy==1.1.0`
  * `matplotlib==3.4.3` 
  * `lmdb==1.2.1` 
  * `pandas==1.3.2` 
  * `sklearn==0.0` 
  * `easydict==1.9` 
  * `torch==1.9.0` 
  * `tensorboardX==2.4`
* Deep Learning network: N/A
* Third Party Code: 
    * https://github.com/jixiaozhong/RealSR
    * https://github.com/Tencent/Real-SR/
    * https://github.com/monaen/LightFieldReconstruction
* Third Party Compute: N/A

# Steps to run the code and the setup
* Fist goto: https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html to install miniconda
* Then run `init_env.sh` to initialize the environment
* Go to `data_download` folder and run `download_datasets.sh` to download the dataset and uncompress it
* ~~Go to `vgg19` folder and run `download_pretrained_vgg19.sh` to download the pretrained weights~~
* Go to the folder `data_processings` and run `stanford.py` to create `lmdb` database of the images -> `Train`, `Test`, and `Validation` and corresponding `csv` files which contains the images path for each Train, Test and Validation images respectively.
* Now the setup is complete
* Now open the terminal, and type: `conda activate tf` - this will activate the python binary and respective packages installed in `tf` environment
* To train the SRGAN_tf run `SRGAN_tf.py` file. 

# More information on the code.
* To train the SRGAN_tf run `SRGAN_tf.py` file. If there be need to change the configuration you can do so same in SRGAN_tf.py file in the __main__ method.
