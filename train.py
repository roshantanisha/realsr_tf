# load all the modules and libraries
from SRGAN_tf import SRGANModel_tf
import yaml
from easydict import EasyDict
import numpy as np
from data_creation_on_fly import *
from tqdm import tqdm

# main function that runs the training and validation of the network.
def main(train=True):
    """
    Args:
        train: whether to do training or testing

    Returns: does not return anything

    """
    # load model and configuration file
    model, conf = load_model()

    #create the data structure for giving it to the model for training and testing
    data = {
        'LQ': None, # LQ means low resolution data point
        'GT': None  # GT mean actual images
    }

    if train:
        # create the data structure for maining the loss value of discriminator and generator for both train and validation phases
        train_log = {'d': [], 'g': []}
        validation_log = {'g': []}

        print(model.conf.network_G.batch_size)
        # build the model for training generator and discriminator
        model.build_model()

        # model.save(0)
        #
        # return None

        # load the train_db and train file
        train_db, train_file = load_data_db(conf.datasets.train)
        # load the validation db and validation file
        validation_db, validation_file = load_data_db(conf.datasets.validation)

        step = 0
        # training for conf.train.epochs times
        for e in range(conf.train.epochs):
            # store the loss value for each step value for each epoch
            temp_d = []
            temp_g = []
            # run through every train files
            for index in range(len(train_file)):
                # patch the actual image for spatial images and angular images
                img, (patches, angular_patches) = load_patches(
                    train_db,
                    index,
                    patch_size=conf.network_G.h_dim*conf.network_G.scale, train=True, sample_patches=1, angular_patch_size=6*conf.network_G.scale
                )

                print(patches.shape, angular_patches.shape)

                # downsample each patch
                labels, patches_lr = downsampling(
                    angular_patches,
                    rs=conf.network_G.scale, ra=conf.network_G.scale
                )

                print(patches_lr.shape, angular_patches.shape)

                # store images in data - data structure for giving it to the model.
                # change the image shape from s, t, h, w to h, w, s, t
                data['LQ'] = np.swapaxes(np.swapaxes(patches_lr, 1, 3), 2, 4)
                data['GT'] = np.swapaxes(np.swapaxes(angular_patches, 1, 3), 2, 4)

                # store the current patch data to the model
                model.feed_data(data)

                # train the model for the current patch and step value
                d_loss, g_loss = model.train_model(step)
                if d_loss is not None:
                    temp_d.append(d_loss)
                if g_loss is not None:
                    temp_g.append(g_loss)
                # do the validation for the current step if validation has to be performed.
                if step % conf.train.validation_step == 0:
                    temp = []
                    for val_index in range(len(validation_file)):
                        img, (patches, angular_patches) = load_patches(
                            train_db,
                            index,
                            patch_size=conf.network_G.h_dim * conf.network_G.scale, train=True, sample_patches=1,
                            angular_patch_size=6 * conf.network_G.scale
                        )

                        print(patches.shape, angular_patches.shape)

                        labels, patches_lr = downsampling(
                            angular_patches,
                            rs=conf.network_G.scale, ra=conf.network_G.scale
                        )

                        print(patches_lr.shape, angular_patches.shape)

                        data['LQ'] = np.swapaxes(np.swapaxes(patches_lr, 1, 3), 2, 4)
                        data['GT'] = np.swapaxes(np.swapaxes(angular_patches, 1, 3), 2, 4)

                        model.feed_data(data)

                        # if validation do not update gradients just get the loss value, here we only get the generator loss value as that is what we are training to upsample the images
                        loss_g = model.train_model(step, validation=True)
                        temp.append(loss_g)

                    validation_log['g'].append(np.mean(temp))
                    del temp

                    break

                step += 1

            train_log['d'].append(np.mean(temp_d))
            train_log['g'].append(np.mean(temp_g))

            del temp_d, temp_g
            # break

        train_db.close()
        validation_db.close()
    else:
        # test the model
        model.load_model(conf)
        # load the test db and the test file
        test_db, test_files = load_data_db(conf.datasets.test)

        # loop through all the test files
        for i in range(len(test_files)):
            img = load_image_from_db(test_db, i)
            (patches, angular_patches) = patch(img, conf.network_G.h_dim, train=False, angular_patch_size=conf.network_G.x_dim)

            returned_img = np.zeros(
                shape=(
                    angular_patches.shape[0],
                    angular_patches.shape[1] * conf.network_G.scale,
                    angular_patches.shape[2] * conf.network_G.scale,
                    angular_patches.shape[3] * conf.network_G.scale,
                    angular_patches.shape[4] * conf.network_G.scale,
                    angular_patches.shape[5]
                )
            )

            index = 0

            # loop through all the patches and store that in the returned images
            for each_angular_patch in tqdm(angular_patches):
                temp = np.swapaxes(np.swapaxes(each_angular_patch, 0, 2), 1, 3)
                data['LQ'] = np.expand_dims(temp, axis=0)
                # this is the testing phase hence we do not require actual images, but we require generator to produce the actual type of images.
                model.feed_data(data, need_GT=False)
                # returns the upsampled image
                temp = model.test()
                temp = np.swapaxes(np.swapaxes(temp, 1, 3), 2, 4)
                returned_img[index:index + 1, :] = temp

            new_img = np.swapaxes(np.swapaxes(returned_img, 1, 3), 2, 4)
            # restore the patches
            completed_image = restore_patches(new_img, returned_img.shape, conf.network_G.upscale)

            # save the upsampled 4D image to the numpy array, this will be like 27GB so you want to take care of HDD/SSD memory while running the testing code.
            np.save(completed_image, 'test_image_' + str(i))


def load_model():
    # load the configuration file
    with open('config.yaml', 'r') as fin:
        conf = EasyDict(yaml.load(fin))

    # load SRGAN model using the configuration file
    model = SRGANModel_tf(conf)
    # model.load_model(conf)

    #return the model and configuration file
    return model, conf


if __name__ == '__main__':
    main(True)
