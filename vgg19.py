import tensorflow as tf
from layers import *


class VGG19:
    # create the VGG19 class modified (Software Engineering only) at from the LightFieldReconstruction Repo.
    def __init__(self, x, t, is_training, graph):
        """
        Args:
            x: input tensor
            t: target tensor
            is_training: whether currently it is training or not - for batch norm
            graph: the tensorflow graph (generator graph)
        """
        # self.graph = tf.Graph()
        self.graph = graph
        # with self.graph.as_default():
        #     self.vgg_graph = tf.Graph()
        if x is None: return
        # we do not wish to build the model now, as we wish to get the vgg model output layer tensor different for both inputs and target (generated) values, while reusing the weights. hence while creating the VGG object we pass None only
        self.out, self.phi = self.build_model(x, is_training)
        self.loss = self.inference_loss(self.out, t)

    def build_model(self, x1, is_training, reuse=False, use_batch_norm=True):
        """
        Args:
            x1: input_tensor
            is_training: whether currently it is training or not?
            reuse: whether to reuse the weights/variables
            use_batch_norm: whether to use batch norm or not for each layer as in VGG19 architecture

        Returns: output tensor, and the features array

        """
        with self.graph.as_default():
            print('Building model...')
            self.use_batch_norm = use_batch_norm
            self.mean = tf.reshape(tf.constant([0.485, 0.456, 0.406]), (1, 1, 1, 3))
            self.std = tf.reshape(tf.constant([0.229, 0.224, 0.225]), (1, 1, 1, 3))
            self.x = (x1 - self.mean) / self.std
            x = self.x
            with tf.variable_scope('vgg19', reuse=reuse):
                self.phi = []
                with tf.variable_scope('conv1a'):
                    x = conv_layer(x, [3, 3, 3, 64], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv1b'):
                    x = conv_layer(x, [3, 3, 64, 64], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                self.phi.append(x)

                x = max_pooling_layer(x, 2, 2)
                with tf.variable_scope('conv2a'):
                    x = conv_layer(x, [3, 3, 64, 128], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv2b'):
                    x = conv_layer(x, [3, 3, 128, 128], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                self.phi.append(x)

                x = max_pooling_layer(x, 2, 2)
                with tf.variable_scope('conv3a'):
                    x = conv_layer(x, [3, 3, 128, 256], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv3b'):
                    x = conv_layer(x, [3, 3, 256, 256], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv3c'):
                    x = conv_layer(x, [3, 3, 256, 256], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv3d'):
                    x = conv_layer(x, [3, 3, 256, 256], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                self.phi.append(x)

                x = max_pooling_layer(x, 2, 2)
                with tf.variable_scope('conv4a'):
                    x = conv_layer(x, [3, 3, 256, 512], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv4b'):
                    x = conv_layer(x, [3, 3, 512, 512], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv4c'):
                    x = conv_layer(x, [3, 3, 512, 512], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv4d'):
                    x = conv_layer(x, [3, 3, 512, 512], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                self.phi.append(x)

                x = max_pooling_layer(x, 2, 2)
                with tf.variable_scope('conv5a'):
                    x = conv_layer(x, [3, 3, 512, 512], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv5b'):
                    x = conv_layer(x, [3, 3, 512, 512], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv5c'):
                    x = conv_layer(x, [3, 3, 512, 512], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                with tf.variable_scope('conv5d'):
                    x = conv_layer(x, [3, 3, 512, 512], 1)
                    if use_batch_norm:
                        x = batch_normalize2d(x, is_training)
                    x = lrelu(x)
                self.phi.append(x)

                x = max_pooling_layer(x, 2, 2)
                x = flatten_layer(x)
                with tf.variable_scope('fc1'):
                    x = full_connection_layer(x, 4096)
                    x = lrelu(x)
                with tf.variable_scope('fc2'):
                    x = full_connection_layer(x, 4096)
                    x = lrelu(x)
                with tf.variable_scope('softmax'):
                    x = full_connection_layer(x, 100)

                output = x
                return output, self.phi


    def inference_loss(self, out, t):
        with self.graph.as_default():
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
                labels=tf.one_hot(t, 100),
                logits=out)
            return tf.reduce_mean(cross_entropy)


    def init_model(self, path):
        self.sess = tf.Session(graph=self.graph)
        with self.graph.as_default():
            var = tf.global_variables()
            vgg_var = []
            for var_ in var:
                print(var_.name, var_.shape)
                if "vgg19" in var_.name:
                    vgg_var.append(var_)
            # print(len(vgg_var))
            # print(vgg_var)
            saver = tf.train.Saver(vgg_var)
            saver.restore(self.sess, path)


class PerceptualLoss:
    def __init__(self, input_shape, cri_loss, use_batch_norm, path, graph):
        self.vgg = VGG19(None, None, None, graph)

        with self.vgg.graph.as_default():
            self.placeholder_input = tf.placeholder(shape=input_shape, dtype=tf.float32)

            self.recons_placeholder = tf.placeholder(shape=input_shape, dtype=tf.float32)

            training = tf.constant(False)

            if cri_loss == 'l1':
                self.cri_loss = tf.losses.absolute_difference
            else:
                self.cri_loss = tf.losses.mean_squared_error

            _, phi1 = self.vgg.build_model(self.placeholder_input, training, False, use_batch_norm)
            _, phi2 = self.vgg.build_model(self.recons_placeholder, training, True, use_batch_norm)

            self.vgg.init_model(path)

            l2_loss = None

            for i in range(len(phi1)):
                if l2_loss == None:
                    l2_loss = self.cri_loss(phi1[i], phi2[i])
                else:
                    l2_loss += self.cri_loss(phi1[i], phi2[i])

            self.loss = tf.reduce_mean(l2_loss)

    def forward(self, input_value, recons_value):
        with self.vgg.graph.as_default():
            loss = self.vgg.sess.run(
                self.loss,
                {
                    self.placeholder_input: input_value,
                    self.recons_placeholder: recons_value,
                }
            )
        return loss


if __name__ == '__main__':
    input_shape = (25, 96, 96, 3)
    cri_loss = 'l1'
    pl = PerceptualLoss(input_shape, cri_loss, False, path='/Users/tanishabhayani/dev/mummy/cm/salah_ab_light_fielf/copy_LRF/LightFieldReconstruction_LapSRN/vgg19/weights/latest', graph=None)
    loss = pl.forward(np.ones(input_shape), np.ones(input_shape))

    print(loss)

