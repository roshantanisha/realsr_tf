import tensorflow as tf
from convolution4d import *
from losses_tf import gan_loss


class ResidualDenseBlock_SC:
    def __init__(self, graph, nf=64, gc=32, bias=True, name='name'):
        """
        Args:
            nf: number of filters
            gc: growth channel
            bias: whether to add bias or not?
        """
        self.nf = nf
        self.gc = gc
        self.bias = bias
        self.graph = graph
        self.name = name
        self.sess = tf.Session(graph=self.graph)

    def build_model(self, input_tensor, prefix_name):
        with self.graph.as_default():
            with tf.name_scope(prefix_name+'ResidualDenseBlock_SC'):
                with tf.name_scope(prefix_name+'conv1'):
                    self.conv1 = conv4d(
                        x=input_tensor,
                        in_channels=self.nf,
                        out_channels=self.gc,
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        bias=self.bias,
                        activation='leaky_relu',
                        name=prefix_name+'_rdb_5c_1_'+self.name,
                        stddev_factor=0.1,
                        graph=self.graph
                    )
                tensor_2 = tf.concat([input_tensor, self.conv1], axis=-1)
                with tf.name_scope(prefix_name+'conv2'):
                    self.conv2 = conv4d(
                        x=tensor_2,
                        in_channels=self.nf + self.gc,
                        out_channels=self.gc,
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        bias=self.bias,
                        activation='leaky_relu',
                        name=prefix_name+'_rdb_5c_2_'+self.name,
                        stddev_factor=0.1,
                        graph=self.graph
                    )
                tensor_3 = tf.concat([tensor_2, self.conv2], axis=-1)
                with tf.name_scope(prefix_name+'conv3'):
                    self.conv3 = conv4d(
                        x=tensor_3,
                        in_channels=self.nf + 2 * self.gc,
                        out_channels=self.gc,
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        bias=self.bias,
                        activation='leaky_relu',
                        name=prefix_name+'_rdb_5c_3_'+self.name,
                        stddev_factor=0.1,
                        graph=self.graph
                    )
                tensor_4 = tf.concat([tensor_3, self.conv3], axis=-1)
                with tf.name_scope(prefix_name+'conv4'):
                    self.conv4 = conv4d(
                        x=tensor_4,
                        in_channels=self.nf + 3*self.gc,
                        out_channels=self.gc,
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        bias=self.bias,
                        activation='leaky_relu',
                        name=prefix_name+'_rdb_5c_4_'+self.name,
                        stddev_factor=0.1,
                        graph=self.graph
                    )
                tensor_5 = tf.concat([tensor_4, self.conv4], axis=-1)
                with tf.name_scope(prefix_name+'conv5'):
                    self.conv5 = conv4d(
                        x=tensor_5,
                        in_channels=self.nf + 4*self.gc,
                        out_channels=self.nf,
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        bias=self.bias,
                        activation='linear',
                        name=prefix_name+'_rdb_5c_5_'+self.name,
                        stddev_factor=0.1,
                        graph=self.graph
                    )

                self.output_tensor = self.conv5 * 0.2 + input_tensor

        return self.output_tensor


class RRDB:
    def __init__(self, nf, graph, gc=32, name='0'):
        self.nf = nf
        self.gc = gc
        self.graph = graph
        self.RDB1 = ResidualDenseBlock_SC(self.graph, self.nf, self.gc, name=name+'rdb1')
        self.RDB2 = ResidualDenseBlock_SC(self.graph, self.nf, self.gc, name=name+'rdb2')
        self.RDB3 = ResidualDenseBlock_SC(self.graph, self.nf, self.gc, name=name+'rdb3')
        self.name = name

    def build_model(self, input_tensor):
        with self.graph.as_default():
            with tf.name_scope(self.name+'RRDB'):
                output1 = self.RDB1.build_model(input_tensor, '1')
                output2 = self.RDB2.build_model(output1, '2')
                output3 = self.RDB3.build_model(output2, '3')

            return output3 * 0.2 + input_tensor


class RRDBNet:
    def __init__(self, in_nc, out_nc, nf, nb, gc=32):
        self.in_nc = in_nc
        self.out_nc = out_nc
        self.nf = nf
        self.nb = nb
        self.gc = gc
        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph)

    def build_model(self, conf):
        with self.graph.as_default():
            self.input_tensor = tf.placeholder(
                shape=(
                    conf.batch_size,
                    conf.x_dim,
                    conf.y_dim,
                    conf.h_dim,
                    conf.w_dim,
                    conf.channels
                ),
                dtype=tf.float32,
                name='input_placeholder_g'
            )
            with tf.name_scope('conv1'):
                self.conv_first = conv4d(
                    x=self.input_tensor,
                    in_channels=self.in_nc,
                    out_channels=self.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True,
                    name='conv_first0',
                    activation='linear',
                    graph=self.graph
                )
            self.block = [self.conv_first]
            self.rrdb = []
            for i in range(self.nb):
                with tf.name_scope('RRDB_block'+str(i)):
                    temp = RRDB(self.nf, self.graph, self.gc, name=str(i))
                    self.rrdb.append(temp)
                    temp = self.rrdb[-1].build_model(self.block[-1])
                    self.block.append(
                        temp
                    )

            with tf.name_scope('Trunk_Conv'):
                self.trunk_conv = conv4d(
                    x=self.block[-1],
                    in_channels=self.nf,
                    out_channels=self.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True,
                    name='trunk_conv1',
                    activation='linear',
                    graph=self.graph
                )

            with tf.name_scope('Upsampling1'):
                self.upconv1 = conv4d_transpose(
                    x=self.conv_first + self.block[-1],
                    in_channels=self.nf,
                    out_channels=self.nf*(2**4),
                    kernel_size_1=1,
                    kernel_size_2=1,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    graph=self.graph,
                    name='upscale_transpose'
                )
                upsample_shape = self.upconv1.shape.as_list()

                self.upconv1_reshaped = tf.reshape(
                    tf.layers.flatten(
                        self.upconv1
                    ),
                    (
                        upsample_shape[0],
                        int(upsample_shape[1] * 2),
                        int(upsample_shape[2] * 2),
                        int(upsample_shape[3] * 2),
                        int(upsample_shape[4] * 2),
                        int(upsample_shape[5] // (2 ** 4))
                    )
                )

                self.upconv1_reshaped_output = tf.nn.leaky_relu(self.upconv1_reshaped, alpha=0.2)

                self.upconv1_reshaped_output_conv = conv4d(
                    x=self.upconv1_reshaped_output,
                    in_channels=self.nf,
                    out_channels=self.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    name='upconv1_reshaped_output_conv',
                    activation='leaky_relu',
                    bias=True,
                    graph=self.graph
                )

            with tf.name_scope('Upsampling2'):
                self.upconv2 = conv4d_transpose(
                    x=self.upconv1_reshaped_output_conv,
                    in_channels=self.nf,
                    out_channels=self.nf * (2 ** 4),
                    kernel_size_1=1,
                    kernel_size_2=1,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    graph=self.graph,
                    name='upscale_transpose2'
                )
                upsample_shape = self.upconv2.shape.as_list()

                self.upconv2_reshaped = tf.reshape(
                    tf.layers.flatten(
                        self.upconv2
                    ),
                    (
                        upsample_shape[0],
                        int(upsample_shape[1] * 2),
                        int(upsample_shape[2] * 2),
                        int(upsample_shape[3] * 2),
                        int(upsample_shape[4] * 2),
                        int(upsample_shape[5] // (2 ** 4))
                    )
                )

                self.upconv2_reshaped_output = tf.nn.leaky_relu(self.upconv1_reshaped, alpha=0.2)

                self.upconv2_reshaped_output_conv = conv4d(
                    x=self.upconv2_reshaped_output,
                    in_channels=self.nf,
                    out_channels=self.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    name='upconv2_reshaped_output_conv',
                    activation='leaky_relu',
                    bias=True,
                    graph=self.graph
                )

            with tf.name_scope('HR_Conv'):
                self.hr_conv = conv4d(
                    x=self.upconv2_reshaped_output_conv,
                    in_channels=self.nf,
                    out_channels=self.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True,
                    name='hr_conv',
                    activation='leaky_relu',
                    graph=self.graph
                )

            with tf.name_scope('Conv_Last'):
                self.conv_last = conv4d(
                    x=self.hr_conv,
                    in_channels=self.nf,
                    out_channels=self.out_nc,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    activation='linear',
                    name='last_conv',
                    graph=self.graph,
                    bias=True
                )

            self.output_tensor = self.conv_last
            self.output_placeholder = tf.placeholder(shape=self.output_tensor.shape, dtype=self.output_tensor.dtype)

        print('here === ', self.output_placeholder)

        return self.output_tensor

    def forward(self, input_value):
        with self.graph.as_default():
            return self.sess.run(
                self.output_tensor,
                {
                    self.input_tensor: input_value
                }
            )

    def backward(self, input_value, output_value, is_target_real, loss_tensor, opt):
        with self.graph.as_default():
            _, loss_value = self.sess.run(
                [opt, loss_tensor],
                {
                    self.input_tensor: input_value,
                    self.output_tensor: output_value
                }
            )
        return loss_value


if __name__ == '__main__':
    model = RRDBNet(in_nc=3, out_nc=3, nf=4, nb=2)

    from easydict import EasyDict
    conf = EasyDict()
    conf.batch_size = 1
    conf.x_dim = 5
    conf.y_dim = 5
    conf.h_dim = 90
    conf.w_dim = 90
    conf.channels = 3

    print(model.build_model(conf))


