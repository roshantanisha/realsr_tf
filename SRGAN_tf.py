import tensorflow as tf
from convolution4d import *
from vgg19 import PerceptualLoss
import networks_tf as networks
from losses_tf import *
import os


# main class for the SRGAN tensorflow based model
class SRGANModel_tf():

    def __init__(self, conf):
        """
        Args:
            conf: configuration file
        """
        # assign the configuration file to the object level variable in the SRGAN_Tf class
        self.conf = conf

    def build_model(self):
        """
        Returns: Nothing creates the model according to the configuration file

        """
        # define generator
        self.G = networks.define_G(self.conf)
        if self.conf['is_train']:
            # is training phase then load the discriminator
            self.D = networks.define_D(self.conf)

        # define losses
        # gan loss
        with self.G.graph.as_default():
            # bool value if the target that we are adding is real (actual image patch) or the fake image patch generated from the generator
            self.G.is_target_real_placeholder = tf.placeholder(shape=(), dtype=tf.bool)
            # the discriiminator output patch that is obtained form the discriminator network.
            self.G.disc_output_placeholder = tf.placeholder(shape=self.D.output_placeholder.shape, dtype=self.D.output_placeholder.dtype)
            # if the generator network weight is present i.e. if the weighted generative loss is to be considered.
            if self.conf.train.gan_weight:
                # get the gan loss
                self.G.gan_loss_tensor = self.conf.train.gan_weight * gan_loss(
                    1,
                    0,
                    self.G.disc_output_placeholder,
                    self.G.is_target_real_placeholder,
                    self.G.graph
                )
            else:
                # if the generator weight is not available, load the simple gan loss
                self.G.gan_loss_tensor = gan_loss(
                    1,
                    0,
                    self.G.disc_output_placeholder,
                    self.G.is_target_real_placeholder,
                    self.G.graph
                )

        with self.G.graph.as_default():
            # perceptual loss
            if self.conf.train.feature_weight > 0:
                # perceptual loss is basically the feature weight since they are obtained form the VGG based netwowrk.
                l_fea_type = self.conf.train.feature_criterion # get the feature type loss - l1 or l2
                # get the generator input shape
                g_shape = self.G.input_tensor.shape.as_list()
                # reshape that to the 2D image, that is collapse the s, t and create the h, w, c images only for VGG model because VGG model is for 2D images and perceptual loss has strong dependency on VGG model,
                int_shape = [None, g_shape[3], g_shape[4], g_shape[5]]
                print('int_shape = ', int_shape)
                # create a Perceptual loss object
                self.perceptual_loss_object = PerceptualLoss(
                    int_shape, # pass the input_shape
                    l_fea_type, # pass the loss type
                    False, # whether to use batch norm
                    self.conf.vgg_path, # pass the weights to the vgg model
                    self.G.graph # pass the generator model graph
                )
                # get the loss tensor value created
                self.perceptual_loss = self.conf.train.feature_weight * self.perceptual_loss_object.loss

        with self.G.graph.as_default():
            #pixel loss
            if self.conf.train.pixel_weight > 0:
                # check pixel level loss between the actual image and the generator output
                if self.conf.train.pixel_criterion == 'l1':
                    self.pixel_loss = self.conf.train.pixel_weight * charbonnier_loss(1e-8, self.G.graph, self.G.output_tensor, self.G.output_placeholder) # l1 loss
                elif self.conf.train.pixel_criterion == 'l1':
                    self.pixel_loss = self.conf.train.pixel_weight * tf.losses.mean_squared_error(self.G.output_placeholder, self.G.output_layer) # l2 lposs

        with self.G.graph.as_default():
            print(self.G.gan_loss_tensor.graph, self.perceptual_loss.graph, self.pixel_loss.graph)
            print(self.perceptual_loss_object.placeholder_input.graph, self.perceptual_loss_object.recons_placeholder.graph)
            # sum all the loss of the generator, it can be weighted or non-weighted depending upon the weights defined in the configuration file.
            self.gan_total_loss = self.G.gan_loss_tensor + self.perceptual_loss + self.pixel_loss

            # define the optimizer for the generator
            self.G.opt = tf.train.AdamOptimizer(
                learning_rate=self.conf.train.lr_G,
                beta1=self.conf.train.beta1_G,
                beta2=self.conf.train.beta2_G,
                name='Adam_G',
            )
            # pass the generator loss to minimize
            self.G.train_opt = self.G.opt.minimize(self.gan_total_loss)

            # initialize the generator network weights
            self.G.sess.run(tf.global_variables_initializer())


        # define the variables for the discriminator network.
        with self.D.graph.as_default():
            # define the real image loss
            self.l_d_real = gan_loss(
                1,
                0,
                self.D.output_layer,
                True,
                self.D.graph
            )
            # define the real image discriminator loss placeholder here for using it in total loss
            self.d_loss_placeholder = tf.placeholder(shape=self.l_d_real.shape, dtype=self.l_d_real.dtype)
            # we create a function for discriminator loss that would take the real image discriminator loss that would give the total loss, this is to map the torch version to tensorflow
            self.l_d_fake = disc_gan_loss(
                1,
                0,
                self.D.output_layer,
                False,
                self.D.graph,
                self.d_loss_placeholder
            )

            self.d_loss = self.l_d_fake

            # define the discriminator optimizer
            self.D.opt = tf.train.AdamOptimizer(
                learning_rate=self.conf.train.lr_D,
                beta1=self.conf.train.beta1_D,
                beta2=self.conf.train.beta2_D,
                name='Adam_D'
            )

            # minimize the the discriminator loss, and get the train opt tensor that is ultimately used for training
            self.D.train_opt = self.D.opt.minimize(self.d_loss)
            # define the discriminator session for running the training
            self.D.sess = tf.Session(graph=self.D.graph)
            # initialize the discriminator weights
            self.D.sess.run(tf.global_variables_initializer())

    def load_model_if_available(self):
        pass

    # save the model, at particular iter steps, this is done whenever validation is done on the network.
    def save(self, iter_step):
        # check whether the train directory exists or not.
        if not os.path.exists('train'):
            # make directory train
            os.makedirs('./train')
        # create the saver for the generator model
        with self.G.graph.as_default():
            saver = tf.train.Saver()
            # save the generator weight and the graph
            saver.save(self.G.sess, 'train/generator', global_step=iter_step)
        # create the saver for the discriminator model
        with self.D.graph.as_default():
            saver = tf.train.Saver()
            # save the discriminator weight and the graph
            saver.save(
                self.D.sess,
                'train/discriminator', global_step=iter_step
            )

    def feed_data(self, data, need_GT=True):
        # save the patch data in the SRGAN_tf class object for training
        self.var_L = data['LQ']
        if need_GT:
            # use this when training and validation is required, other wise for testing purpose this is not used.
            self.var_H = data['GT']
            self.var_ref = self.var_H

    def load_model(self, conf):
        # use this function while doing testing on the dataset.
        self.G = networks.define_G(self.conf)
        # load the generator network.
        with self.G.graph.as_default():
            # saver = tf.train.import_meta_graph(conf.model.network_G_path + '.meta')
            saver = tf.train.Saver()
            # restore the weights of the generator network.
            saver.restore(self.G.sess, conf.model.network_G_path)

            for var in tf.all_variables():
                print(var.name)

    # def test_model(self, input_img):
        # with self.sess_g.graph.as_default():
        #     returned_value = self.sess_g.run(
        #         self.sess_g.
        #     )

    def train_model(self, step, validation=False):
        """
        Args:
            step: current global step in training
            validation: whether this step includes validation or not?

        Returns: loss values

        """
        # get the generated values from the downsampled image
        self.fake_H = self.G.forward(self.var_L)
        print(self.var_L.shape)
        print(self.fake_H.shape)
        print(self.var_H.shape)

        # if the current step is for the validation step then do validation
        if step % self.conf.train.validation_step == 0 and validation:
            # save the model at the validation step
            self.save(step)
            with self.G.graph.as_default():
                # get the fake image from the downsampled image
                self.fake_H = self.G.sess.run(
                    self.G.output_tensor,
                    {
                        self.G.input_tensor: self.var_L
                    }
                )
            with self.D.graph.as_default():
                # get the discriminator output for the generated/fakae image
                d_output = self.D.sess.run(
                    self.D.output_layer,
                    {
                        self.D.input_tensor: self.fake_H
                    }
                )
            with self.G.graph.as_default():
                # reshape the input tensor for the perceptual loss here
                int_shape = [-1, 96, 96, 3]
                placeholder_input = np.reshape(self.var_H, int_shape)
                recons_input = np.reshape(self.fake_H, int_shape)
                # get the total generator loss and train the generator
                _, loss_g = self.G.sess.run(
                    [self.G.train_opt, self.gan_total_loss],
                    {
                        self.G.input_tensor: self.var_L,
                        self.G.output_placeholder: self.var_H,
                        self.G.disc_output_placeholder: d_output,
                        self.G.is_target_real_placeholder: False,
                        self.perceptual_loss_object.placeholder_input: placeholder_input,
                        self.perceptual_loss_object.recons_placeholder: recons_input
                    }
                )
                return loss_g
        else:
            # do the training
            with self.D.graph.as_default():
                # get the loss for the real
                l_d_real = self.D.sess.run(
                    self.l_d_real,
                    {
                        self.D.input_tensor: self.var_H
                    }
                )
                # the discriminator loss is: 0.5*(real_loss + fake_loss)
                # get the loss for the discriminator
                _, loss = self.D.sess.run(
                    [self.D.train_opt, self.d_loss],
                    {
                        self.D.input_tensor: self.fake_H,
                        self.d_loss_placeholder: l_d_real
                    }
                )

                print('Discriminator: ', loss)

            if step % self.conf.train.D_update_ratio == 0 and step > self.conf.train.D_init_iters:
                with self.D.graph.as_default():
                    # get the discriminator output for the fake image generated from the generator
                    d_output = self.D.sess.run(
                        self.D.output_layer,
                        {
                            self.D.input_tensor: self.fake_H
                        }
                    )
                with self.G.graph.as_default():
                    int_shape = [-1, 96, 96, 3]
                    placeholder_input = np.reshape(self.var_H, int_shape)
                    recons_input = np.reshape(self.fake_H, int_shape)
                    # get the loss associated with the vgg features that is the perceptual loss
                    _, loss_g = self.G.sess.run(
                        [self.G.train_opt, self.gan_total_loss],
                        {
                            self.G.input_tensor: self.var_L,
                            self.G.output_placeholder: self.var_H,
                            self.G.disc_output_placeholder: d_output,
                            self.G.is_target_real_placeholder: False,
                            self.perceptual_loss_object.placeholder_input: placeholder_input,
                            self.perceptual_loss_object.recons_placeholder: recons_input
                        }
                    )

                    print('Generator: ', loss_g)
            else:
                loss_g = None

            # return the discriminator and generator loss in the end.
            return loss, loss_g

    def test(self):
        # self.sess_g.run(
        #     'weight_last_conv: 0',
        #     {
        #
        #     }
        # )
        # for testing just do generator forward lower resolution image.
        temp = self.G.forward(self.var_L)
        return temp
