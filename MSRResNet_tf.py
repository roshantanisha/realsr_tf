import tensorflow as tf
from convolution4d import *


def residual_block_no_nb(input_tensor, index, nf=64, graph=None):
    """
    Args:
        input_tensor: input tensor
        nf: number of filters
        graph: graph corresponding to the model

    Returns: creates: ---Conv-ReLU-Conv-- and returns the residual output of it.
    """
    with graph.as_default():
        with tf.name_scope('residual_block_no_nb_' + str(index)):
            conv1 = conv4d(
                x=input_tensor,
                in_channels=nf,
                out_channels=nf,
                kernel_size_1=3,
                kernel_size_2=3,
                stride_1=1,
                stride_2=1,
                bias=True,
                activation='relu',
                graph=graph,
                name='rb_norb0' + str(index)
            )
            conv2 = conv4d(
                x=conv1,
                in_channels=nf,
                out_channels=nf,
                kernel_size_1=3,
                kernel_size_2=3,
                stride_1=1,
                stride_2=1,
                bias=True,
                activation='linear',
                graph=graph,
                name='rb_norb1' + str(index)
            )

        return input_tensor + conv2


class MSRResNet:
    def __init__(self, in_nc=3, out_nc=3, nf=64, nb=16, upscale=4):
        self.graph = tf.Graph()
        self.upscale = upscale
        self.in_nc = in_nc
        self.out_nc = out_nc
        self.nf = nf
        self.nb = nb
        self.sess = tf.Session(graph=self.graph)

    def build_model(self, conf):
        with self.graph.as_default():
            self.input_tensor = tf.placeholder(
                shape=(conf.batch_size, conf.x_dim, conf.y_dim, conf.h_dim, conf.w_dim, conf.channels),
                dtype=tf.float32
            )
            self.block = []
            with tf.name_scope('conv1'):
                temp = conv4d(
                        x=self.input_tensor,
                        in_channels=self.in_nc,
                        out_channels=self.nf,
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        bias=True,
                        stddev_factor=0.1,
                        graph=self.graph,
                        name='block0'
                    )
                self.block.append(
                    temp
                )

            for i in range(self.nb):
                self.block.append(
                    residual_block_no_nb(
                        self.block[-1],
                        index=i,
                        nf=self.nf,
                        graph=self.graph
                    )
                )

            if self.upscale == 2:
                with tf.name_scope('upscale_2_conv'):
                    self.upconv1 = conv4d(
                        x=self.block[-1],
                        in_channels=self.nf,
                        out_channels=self.nf * (2 ** 4),
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        padding='SAME',
                        bias=True,
                        stddev_factor=0.1,
                        graph=self.graph,
                        name='sclae0'
                    )
                    upconv1_shape = self.upconv1.shape.as_list()
                    self.pixel_shuffle1 = tf.reshape(
                        tf.layers.flatten(
                            self.upconv1
                        ),
                        (
                            upconv1_shape[0],
                            int(upconv1_shape[1] * 2),
                            int(upconv1_shape[2] * 2),
                            int(upconv1_shape[3] * 2),
                            int(upconv1_shape[4] * 2),
                            int(upconv1_shape[5] // (2**4))
                        )
                    )
            elif self.upscale == 3:
                with tf.name_scope('upscale_3_conv'):
                    self.upconv1 = conv4d(
                        x=self.block[-1],
                        in_channels=self.nf,
                        out_channels=self.nf * (3 ** 4),
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        padding='SAME',
                        bias=True,
                        stddev_factor=0.1,
                        graph=self.graph,
                        name='scale1'
                    )
                    upconv1_shape = self.upconv1.shape.as_list()
                    self.pixel_shuffle1 = tf.reshape(
                        tf.layers.flatten(
                            self.upconv1
                        ),
                        (
                            upconv1_shape[0],
                            int(upconv1_shape[1] * (3**4)),
                            int(upconv1_shape[2] * (3**4)),
                            int(upconv1_shape[3] * (3**4)),
                            int(upconv1_shape[4] * (3**4)),
                            int(upconv1_shape[5] // (3**4))
                        )
                    )
            elif self.upscale == 4:
                with tf.name_scope('upscale_4_conv'):
                    self.upconv1 = conv4d(
                        x=self.block[-1],
                        in_channels=self.nf,
                        out_channels=self.nf * (4 ** 4),
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        padding='SAME',
                        bias=True,
                        graph=self.graph,
                        name='scale0'
                    )
                    self.upconv2 = conv4d(
                        x=self.upconv1,
                        in_channels=self.nf,
                        out_channels=self.nf * (4 ** 4),
                        kernel_size_1=3,
                        kernel_size_2=3,
                        stride_1=1,
                        stride_2=1,
                        padding='SAME',
                        bias=True,
                        stddev_factor=0.1,
                        graph=self.graph,
                        name='scale1'
                    )
                    upconv1_shape = self.upconv2.shape.as_list()
                    self.pixel_shuffle1 = tf.reshape(
                        tf.layers.flatten(
                            self.upconv2
                        ),
                        (
                            upconv1_shape[0],
                            int(upconv1_shape[1] * 4),
                            int(upconv1_shape[2] * 4),
                            int(upconv1_shape[3] * 4),
                            int(upconv1_shape[4] * 4),
                            int(upconv1_shape[5] // (4**4))
                        )
                    )

            with tf.name_scope('HR_conv'):
                self.HRconv = conv4d(
                    x=self.pixel_shuffle1,
                    in_channels=self.nf,
                    out_channels=self.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True,
                    stddev_factor=0.1,
                    graph=self.graph,
                    name='hrconv'
                )

                self.lrelu1 = tf.nn.leaky_relu(
                    self.HRconv,
                    0.1
                )

            with tf.name_scope('last_conv'):
                self.last_conv = conv4d(
                    x=self.lrelu1,
                    in_channels=self.nf,
                    out_channels=self.out_nc,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True,
                    stddev_factor=0.1,
                    graph=self.graph,
                    name='last_conv'
                )

                self.upsample_input = conv4d_transpose(
                    x=self.input_tensor,
                    in_channels=self.in_nc,
                    out_channels=self.out_nc*(self.upscale ** 4),
                    kernel_size_1=1,
                    kernel_size_2=1,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    graph=self.graph,
                    name='upsample_transpose'
                )

                upsample_shape = self.upsample_input.shape.as_list()

                self.final_pixel_shuffle = tf.reshape(
                        tf.layers.flatten(
                            self.upsample_input
                        ),
                        (
                            upsample_shape[0],
                            int(upsample_shape[1] * self.upscale),
                            int(upsample_shape[2] * self.upscale),
                            int(upsample_shape[3] * self.upscale),
                            int(upsample_shape[4] * self.upscale),
                            int(upsample_shape[5] // (self.upscale**4))
                        )
                    )

            print('here == ', self.last_conv.shape, self.final_pixel_shuffle.shape, self.input_tensor.shape)

            self.output_tensor = self.last_conv + self.final_pixel_shuffle
            self.output_placeholder = tf.placeholder(shape=self.output_tensor.shape, dtype=self.output_tensor.dtype)

            return self.output_tensor


if __name__ == '__main__':
    from easydict import EasyDict

    conf = EasyDict()
    conf.batch_size=1
    conf.x_dim=5
    conf.y_dim=5
    conf.h_dim=90
    conf.w_dim=90
    conf.channels=3

    model = MSRResNet()
    model.build_model(conf)
