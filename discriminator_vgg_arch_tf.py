import tensorflow as tf
from convolution4d import *


def batchnorm(input_tensor, graph=None):
    with graph.as_default():
        return tf.nn.batch_normalization(
            x=input_tensor,
            mean=tf.reduce_mean(input_tensor),
            variance=tf.math.reduce_variance(input_tensor),
            offset=0,
            scale=1,
            variance_epsilon=1e-5,
        )


class NLayerDiscriminator():
    """
    Defines a PatchGAN Discriminator
    """
    def __init__(self, input_nc, ndf=64, n_layers=3, conf=None):
        """
        Args:
            input_nc: the number of channels in input images
            ndf: the number of filters in the last conv layer
            n_layers: the number of conv layers in the discriminator
        """
        self.graph = tf.Graph()
        self.build_model(input_nc, ndf, n_layers, conf)

    def build_model(self, input_nc, ndf, n_layers, conf):
        with self.graph.as_default():
            self.input_tensor = tf.placeholder(shape=(conf.batch_size, conf.x_dim, conf.y_dim, conf.h_dim, conf.w_dim, input_nc), dtype=tf.float32)
            use_bias = False
            kw = 4
            with tf.name_scope('conv1'):
                temp = conv4d(
                        x=self.input_tensor,
                        in_channels=input_nc,
                        out_channels=ndf,
                        kernel_size_1=kw,
                        kernel_size_2=kw,
                        stride_1=2,
                        stride_2=2,
                        bias=True,
                        graph=self.graph,
                        name='0',
                        activation='leaky_relu'
                    )
                self.sequence = [
                    temp
                ]

            nf_mult = 1
            nf_mult_prev = 1

            for i in range(1, n_layers):
                nf_mult_prev = nf_mult
                nf_mult = min(2**i, 8)

                with tf.name_scope('n_layer'+str(i)):
                    self.sequence.append(
                        conv4d(
                            x=self.sequence[-1],
                            in_channels=ndf * nf_mult_prev,
                            out_channels=ndf * nf_mult,
                            kernel_size_1=kw,
                            kernel_size_2=kw,
                            stride_1=2,
                            stride_2=2,
                            bias=use_bias,
                            graph=self.graph,
                            name=str(i)
                        )
                    )
                    self.sequence.append(
                        batchnorm(self.sequence[-1], self.graph)
                    )
                    self.sequence.append(
                        tf.nn.leaky_relu(self.sequence[-1])
                    )

            nf_mult_prev = nf_mult
            nf_mult = min(2 ** n_layers, 8)

            with tf.name_scope('n_layer' + str(n_layers + 1)):
                self.sequence.append(
                    conv4d(
                        x=self.sequence[-1],
                        in_channels=ndf*nf_mult_prev,
                        out_channels=ndf*nf_mult,
                        kernel_size_1=kw,
                        kernel_size_2=kw,
                        stride_1=1,
                        stride_2=1,
                        bias=use_bias,
                        graph=self.graph,
                        name=str(n_layers)
                    )
                )
                self.sequence.append(
                    batchnorm(self.sequence[-1], self.graph)
                )
                self.sequence.append(
                    tf.nn.leaky_relu(self.sequence[-1])
                )

            with tf.name_scope('last_conv'):
                self.sequence.append(
                    conv4d(
                        x=self.sequence[-1],
                        in_channels=ndf*nf_mult,
                        out_channels=1,
                        kernel_size_1=kw,
                        kernel_size_2=kw,
                        stride_1=1,
                        stride_2=1,
                        graph=self.graph,
                        name=str(n_layers + 1)
                    )
                )

            self.output_layer = self.sequence[-1]
            self.output_placeholder = tf.placeholder(shape=self.output_layer.shape, dtype=self.output_layer.dtype)

    def forward(self, input_value, sess):
        with self.graph.as_default():
            output_map = sess.run(
                self.output_layer,
                {
                    self.input_tensor: input_value
                }
            )
        return output_map

    def backward(self, input_value, output_value, sess, opt, loss_tensor):
        with self.graph.as_default():
            _, loss_value = sess.run(
                [opt, loss_tensor],
                {
                    self.input_tensor: input_value,
                    self.output_placeholder: output_value
                }
            )
        return loss_value


class Discriminator_VGG_128:
    def __init__(self, conf):
        self.conf = conf
        self.graph = tf.Graph()
        self.build_model()

    def build_model(self):
        with self.graph.as_default():
            self.input_tensor = tf.placeholder(shape=(None, self.conf.x_dim, self.conf.y_dim, self.conf.h_dim, self.conf.w_dim, self.conf.channels), dtype=tf.float32)
            with tf.name_scope('block1'):
                # [64, 128, 128]
                self.conv0_0 = conv4d(
                    x=self.input_tensor,
                    in_channels=self.conf.in_nc,
                    out_channels=self.conf.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True,
                    activation='linear',
                    graph=self.graph,
                    name='00'
                )

                self.activation = tf.nn.leaky_relu(
                    self.conv0_0
                )

                self.conv0_1 = conv4d(
                    x=self.activation,
                    in_channels=self.conf.nf,
                    out_channels=self.conf.nf,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='01'
                )
                self.bn0_1 = batchnorm(
                    self.conv0_1, self.graph
                )
                self.activation0 = tf.nn.leaky_relu(self.bn0_1)

            with tf.name_scope('block2'):
                # [64, 64, 64]
                self.conv1_0 = conv4d(
                    x=self.activation0,
                    in_channels=self.conf.nf,
                    out_channels=self.conf.nf * 2,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='10'
                )

                self.bn1_0 = batchnorm(
                    self.conv1_0, self.graph
                )

                self.activation1 = tf.nn.leaky_relu(self.bn1_0)

                self.conv1_1 = conv4d(
                    x=self.activation1,
                    in_channels=self.conf.nf * 2,
                    out_channels=self.conf.nf * 2,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='11'
                )

                self.bn1_1 = batchnorm(
                    self.conv1_1, self.graph
                )

                self.activation1_1 = tf.nn.leaky_relu(self.bn1_1)

            with tf.name_scope('block3'):
                # [128, 32, 32]
                self.conv2_0 = conv4d(
                    x=self.activation1_1,
                    in_channels=self.conf.nf * 2,
                    out_channels=self.conf.nf * 4,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='20'
                )

                self.bn2_0 = batchnorm(
                    self.conv2_0,
                    self.graph
                )

                self.activation2_0 = tf.nn.leaky_relu(self.bn2_0)

                self.conv2_1 = conv4d(
                    x=self.activation2_0,
                    in_channels=self.conf.nf * 4,
                    out_channels=self.conf.nf * 4,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='21'
                )

                self.bn2_1 = batchnorm(
                    self.conv2_1,
                    self.graph
                )

                self.activation2_1 = tf.nn.leaky_relu(self.bn2_1)

            with tf.name_scope('block4'):
                # [256, 16, 16]
                self.conv3_0 = conv4d(
                    x=self.activation2_1,
                    in_channels=self.conf.nf * 4,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='30'
                )

                self.bn3_0 = batchnorm(self.conv3_0, self.graph)

                self.activation3_0 = tf.nn.leaky_relu(self.bn3_0)

                self.conv3_1 = conv4d(
                    x=self.activation3_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='31'
                )

                self.bn3_1 = batchnorm(self.conv3_1, self.graph)

                self.activation3_1 = tf.nn.leaky_relu(self.bn3_1)

            with tf.name_scope('block5'):
                self.conv4_0 = conv4d(
                    x=self.activation3_1,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='40'
                )

                self.bn4_0 = batchnorm(self.conv4_0, self.graph)

                self.activation4_0 = tf.nn.leaky_relu(self.bn4_0)

                self.conv4_1 = conv4d(
                    x=self.activation4_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    graph=self.graph,
                    name='41'
                )

                self.bn4_1 = batchnorm(self.conv4_1, self.graph)

                self.activation4_1 = tf.nn.leaky_relu(self.bn4_1)

            with tf.name_scope('block6'):
                act_shape = self.activation4_1.shape.as_list()
                self.reshape_layer = tf.reshape(self.activation4_1, (-1, act_shape[1]*act_shape[2]*act_shape[3]*act_shape[4]*act_shape[5]))
                self.fc1 = tf.contrib.layers.fully_connected(
                    self.reshape_layer, 100
                )

                self.activation_fc1 = tf.nn.leaky_relu(
                    self.fc1
                )

                self.fc2 = tf.contrib.layers.fully_connected(
                    self.activation_fc1, 1, None
                )

            self.output_layer = self.fc2
            self.output_placeholder = tf.placeholder(shape=self.output_layer.shape, dtype=self.output_layer.dtype)

    def forward(self, input_value, sess):
        with self.graph.as_default():
            output_value = sess.run(
                self.output_layer,
                {
                    self.input_tensor: input_value
                }
            )

        return output_value

    def backward(self, input_value, output_value, sess, opt, loss_tensor):
        with self.graph.as_default():
            _, loss_value = sess.run(
                [opt, loss_tensor],
                {
                    self.input_tensor: input_value,
                    self.output_placeholder: output_value
                }
            )
        return loss_value


class Discriminator_VGG_256:
    def __init__(self, conf):
        self.conf = conf
        self.graph = tf.Graph()
        self.build_model()

    def build_model(self):
        with self.graph.as_default():
            self.input_tensor = tf.placeholder(shape=(None, 6, 6, 64, 64, 3), dtype=tf.float32)
            with tf.name_scope('block1'):
                # [64, 128, 128]
                self.conv0_0 = conv4d(
                    x=self.input_tensor,
                    in_channels=self.conf.in_nc,
                    out_channels=self.conf.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True,
                    activation='linear',
                    name='00',
                    graph=self.graph
                )

                self.activation = tf.nn.leaky_relu(
                    self.conv0_0
                )

                self.conv0_1 = conv4d(
                    x=self.activation,
                    in_channels=self.conf.nf,
                    out_channels=self.conf.nf,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='01',
                    graph=self.graph
                )
                self.bn0_1 = batchnorm(
                    self.conv0_1, self.graph
                )
                self.activation0 = tf.nn.leaky_relu(self.bn0_1)

            with tf.name_scope('block2'):
                # [64, 64, 64]
                self.conv1_0 = conv4d(
                    x=self.activation0,
                    in_channels=self.conf.nf,
                    out_channels=self.conf.nf * 2,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='10',
                    graph=self.graph
                )

                self.bn1_0 = batchnorm(
                    self.conv1_0, self.graph
                )

                self.activation1 = tf.nn.leaky_relu(self.bn1_0)

                self.conv1_1 = conv4d(
                    x=self.activation1,
                    in_channels=self.conf.nf * 2,
                    out_channels=self.conf.nf * 2,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='11',
                    graph=self.graph
                )

                self.bn1_1 = batchnorm(
                    self.conv1_1, self.graph
                )

                self.activation1_1 = tf.nn.leaky_relu(self.bn1_1)

            with tf.name_scope('block3'):
                # [128, 32, 32]
                self.conv2_0 = conv4d(
                    x=self.activation1_1,
                    in_channels=self.conf.nf * 2,
                    out_channels=self.conf.nf * 4,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='20',
                    graph=self.graph
                )

                self.bn2_0 = batchnorm(
                    self.conv2_0,
                    self.graph
                )

                self.activation2_0 = tf.nn.leaky_relu(self.bn2_0)

                self.conv2_1 = conv4d(
                    x=self.activation2_0,
                    in_channels=self.conf.nf * 4,
                    out_channels=self.conf.nf * 4,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='21',
                    graph=self.graph
                )

                self.bn2_1 = batchnorm(
                    self.conv2_1,
                    self.graph
                )

                self.activation2_1 = tf.nn.leaky_relu(self.bn2_1)

            with tf.name_scope('block4'):
                # [256, 16, 16]
                self.conv3_0 = conv4d(
                    x=self.activation2_1,
                    in_channels=self.conf.nf * 4,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='30',
                    graph=self.graph
                )

                self.bn3_0 = batchnorm(self.conv3_0, self.graph)

                self.activation3_0 = tf.nn.leaky_relu(self.bn3_0)

                self.conv3_1 = conv4d(
                    x=self.activation3_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='31',
                    graph=self.graph
                )

                self.bn3_1 = batchnorm(self.conv3_1, self.graph)

                self.activation3_1 = tf.nn.leaky_relu(self.bn3_1)

            with tf.name_scope('block5'):
                self.conv4_0 = conv4d(
                    x=self.activation3_1,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='40',
                    graph=self.graph
                )

                self.bn4_0 = batchnorm(self.conv4_0, self.graph)

                self.activation4_0 = tf.nn.leaky_relu(self.bn4_0)

                self.conv4_1 = conv4d(
                    x=self.activation4_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='41',
                    graph=self.graph
                )

                self.bn4_1 = batchnorm(self.conv4_1, self.graph)

                self.activation4_1 = tf.nn.leaky_relu(self.bn4_1)

            with tf.name_scope('block6'):
                self.conv5_0 = conv4d(
                    x=self.activation4_1,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='50',
                    graph=self.graph
                )

                self.bn5_0 = batchnorm(self.conv5_0, self.graph)

                self.activation5_0 = tf.nn.leaky_relu(self.bn5_0)

                self.conv5_1 = conv4d(
                    x=self.activation5_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='51',
                    graph=self.graph
                )

                self.bn5_1 = batchnorm(self.conv5_1, self.graph)

                self.activation5_1 = tf.nn.leaky_relu(self.bn5_1)

            with tf.name_scope('block7'):
                act_shape = self.activation5_1.shape.as_list()
                self.reshape_layer = tf.reshape(self.activation5_1, (-1, act_shape[1]*act_shape[2]*act_shape[3]*act_shape[4]*act_shape[5]))
                self.fc1 = tf.contrib.layers.fully_connected(
                    self.reshape_layer, 100
                )

                self.activation_fc1 = tf.nn.leaky_relu(
                    self.fc1
                )

                self.fc2 = tf.contrib.layers.fully_connected(
                    self.activation_fc1, 1, None
                )

            self.output_layer = self.fc2
            self.output_placeholder = tf.placeholder(shape=self.output_layer.shape, dtype=self.output_layer.dtype)

    def forward(self, input_value, sess):
        with self.graph.as_default():
            output_value = sess.run(
                self.output_layer,
                {
                    self.input_tensor: input_value
                }
            )

        return output_value

    def backward(self, input_value, output_value, sess, opt, loss_tensor):
        with self.graph.as_default():
            _, loss_value = sess.run(
                [opt, loss_tensor],
                {
                    self.input_tensor: input_value,
                    self.output_placeholder: output_value
                }
            )
        return loss_value


class Discriminator_VGG_512:
    def __init__(self, conf):
        self.conf = conf
        self.graph = tf.Graph()
        self.build_model()

    def build_model(self):
        with self.graph.as_default():
            self.input_tensor = tf.placeholder(shape=(None, 6, 6, 64, 64, 3), dtype=tf.float32)
            with tf.name_scope('block1'):
                # [64, 128, 128]
                self.conv0_0 = conv4d(
                    x=self.input_tensor,
                    in_channels=self.conf.in_nc,
                    out_channels=self.conf.nf,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=True,
                    activation='linear',
                    name='00',
                    graph=self.graph
                )

                self.activation = tf.nn.leaky_relu(
                    self.conv0_0
                )

                self.conv0_1 = conv4d(
                    x=self.activation,
                    in_channels=self.conf.nf,
                    out_channels=self.conf.nf,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='01',
                    graph=self.graph
                )
                self.bn0_1 = batchnorm(
                    self.conv0_1, self.graph
                )
                self.activation0 = tf.nn.leaky_relu(self.bn0_1)

            with tf.name_scope('block2'):
                # [64, 64, 64]
                self.conv1_0 = conv4d(
                    x=self.activation0,
                    in_channels=self.conf.nf,
                    out_channels=self.conf.nf * 2,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='10',
                    graph=self.graph
                )

                self.bn1_0 = batchnorm(
                    self.conv1_0, self.graph
                )

                self.activation1 = tf.nn.leaky_relu(self.bn1_0)

                self.conv1_1 = conv4d(
                    x=self.activation1,
                    in_channels=self.conf.nf * 2,
                    out_channels=self.conf.nf * 2,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='11',
                    graph=self.graph
                )

                self.bn1_1 = batchnorm(
                    self.conv1_1, self.graph
                )

                self.activation1_1 = tf.nn.leaky_relu(self.bn1_1)

            with tf.name_scope('block3'):
                # [128, 32, 32]
                self.conv2_0 = conv4d(
                    x=self.activation1_1,
                    in_channels=self.conf.nf * 2,
                    out_channels=self.conf.nf * 4,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='20',
                    graph=self.graph
                )

                self.bn2_0 = batchnorm(
                    self.conv2_0,
                    self.graph
                )

                self.activation2_0 = tf.nn.leaky_relu(self.bn2_0)

                self.conv2_1 = conv4d(
                    x=self.activation2_0,
                    in_channels=self.conf.nf * 4,
                    out_channels=self.conf.nf * 4,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='21',
                    graph=self.graph
                )

                self.bn2_1 = batchnorm(
                    self.conv2_1,
                    self.graph
                )

                self.activation2_1 = tf.nn.leaky_relu(self.bn2_1)

            with tf.name_scope('block4'):
                # [256, 16, 16]
                self.conv3_0 = conv4d(
                    x=self.activation2_1,
                    in_channels=self.conf.nf * 4,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='30',
                    graph=self.graph
                )

                self.bn3_0 = batchnorm(self.conv3_0, self.graph)

                self.activation3_0 = tf.nn.leaky_relu(self.bn3_0)

                self.conv3_1 = conv4d(
                    x=self.activation3_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='31',
                    graph=self.graph
                )

                self.bn3_1 = batchnorm(self.conv3_1, self.graph)

                self.activation3_1 = tf.nn.leaky_relu(self.bn3_1)

            with tf.name_scope('block5'):
                self.conv4_0 = conv4d(
                    x=self.activation3_1,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='40',
                    graph=self.graph
                )

                self.bn4_0 = batchnorm(self.conv4_0, self.graph)

                self.activation4_0 = tf.nn.leaky_relu(self.bn4_0)

                self.conv4_1 = conv4d(
                    x=self.activation4_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='41',
                    graph=self.graph
                )

                self.bn4_1 = batchnorm(self.conv4_1, self.graph)

                self.activation4_1 = tf.nn.leaky_relu(self.bn4_1)

            with tf.name_scope('block6'):
                self.conv5_0 = conv4d(
                    x=self.activation4_1,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='50',
                    graph=self.graph
                )

                self.bn5_0 = batchnorm(self.conv5_0, self.graph)

                self.activation5_0 = tf.nn.leaky_relu(self.bn5_0)

                self.conv5_1 = conv4d(
                    x=self.activation5_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='51',
                    graph=self.graph
                )

                self.bn5_1 = batchnorm(self.conv5_1, self.graph)

                self.activation5_1 = tf.nn.leaky_relu(self.bn5_1)

            with tf.name_scope('block7'):
                self.conv6_0 = conv4d(
                    x=self.activation5_1,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=3,
                    kernel_size_2=3,
                    stride_1=1,
                    stride_2=1,
                    bias=False,
                    activation='linear',
                    name='60',
                    graph=self.graph
                )

                self.bn6_0 = batchnorm(self.conv6_0, self.graph)

                self.activation6_0 = tf.nn.leaky_relu(self.bn6_0)

                self.conv6_1 = conv4d(
                    x=self.activation6_0,
                    in_channels=self.conf.nf * 8,
                    out_channels=self.conf.nf * 8,
                    kernel_size_1=4,
                    kernel_size_2=4,
                    stride_1=2,
                    stride_2=2,
                    bias=False,
                    activation='linear',
                    name='61',
                    graph=self.graph
                )

                self.bn6_1 = batchnorm(self.conv6_1, self.graph)

                self.activation6_1 = tf.nn.leaky_relu(self.bn6_1)

            with tf.name_scope('block8'):
                act_shape = self.activation6_1.shape.as_list()
                self.reshape_layer = tf.reshape(self.activation6_1, (-1, act_shape[1]*act_shape[2]*act_shape[3]*act_shape[4]*act_shape[5]))
                self.fc1 = tf.contrib.layers.fully_connected(
                    self.reshape_layer, 100
                )

                self.activation_fc1 = tf.nn.leaky_relu(
                    self.fc1
                )

                self.fc2 = tf.contrib.layers.fully_connected(
                    self.activation_fc1, 1, None
                )

            self.output_layer = self.fc2
            self.output_placeholder = tf.placeholder(shape=self.output_layer.shape, dtype=self.output_layer.dtype)

    def forward(self, input_value, sess):
        with self.graph.as_default():
            output_value = sess.run(
                self.output_layer,
                {
                    self.input_tensor: input_value
                }
            )

        return output_value

    def backward(self, input_value, output_value, sess, opt, loss_tensor):
        with self.graph.as_default():
            _, loss_value = sess.run(
                [opt, loss_tensor],
                {
                    self.input_tensor: input_value,
                    self.output_placeholder: output_value
                }
            )
        return loss_value
