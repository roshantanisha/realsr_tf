import tensorflow as tf


def charbonnier_loss(eps, graph, x, y):
    """
    Args:
        eps: a small number to avoid getting zero in the loss value
        graph: the tensorflow graph associated with generator
        x: the actual image variable
        y: the target / predicted variable

    This is used in the pixel loss value

    Returns: the loss tensor computing the l1 norm

    """
    with graph.as_default():
        diff = x - y
        loss = tf.reduce_sum(tf.sqrt(tf.square(diff + eps)))
    return loss


def gan_loss(real_label_val, fake_label_val, input, target_is_real, graph):
    """
    Args:
        real_label_val: real label value means whether real image class is 1/0
        fake_label_val: fake label value means whether real image class is 1/0
        input: the predicted value from generator (fake) or the actual input image (real)
        target_is_real: if the target is real or not?
        graph: the graph of the discriminator/generator

    Returns: the loss tensor

    """
    with graph.as_default():
        if target_is_real == tf.constant(True):
            if real_label_val == 1:
                target_label = tf.ones_like(input)
            else:
                target_label = tf.zeros_like(input)
        else:
            if fake_label_val == 1:
                target_label = tf.ones_like(input)
            else:
                target_label = tf.zeros_like(input)
        # calculates the sigmoid cross entropy loss between the input and the target (1/0) depending on what the input is - whether real or fake
        loss = tf.losses.sigmoid_cross_entropy(input, target_label)

    return loss


def disc_gan_loss(real_label_val, fake_label_val, input, target_is_real, graph, loss_value1):
    """
    Args:
        real_label_val:real label value means whether real image class is 1/0
        fake_label_val:fake label value means whether real image class is 1/0
        input:the predicted value from generator (fake) or the actual input image (real)
        target_is_real:if the target is real or not?
        graph:the graph of the discriminator
        loss_value1: the loss tensor from the above loss function to calculate the summation of the real discriminator + fake discriminator loss

    Returns: the final discriminator loss tensor

    """
    with graph.as_default():
        if target_is_real == tf.constant(True):
            if real_label_val == 1:
                target_label = tf.ones_like(input)
            else:
                target_label = tf.zeros_like(input)
        else:
            if fake_label_val == 1:
                target_label = tf.ones_like(input)
            else:
                target_label = tf.zeros_like(input)
        loss = tf.losses.sigmoid_cross_entropy(input, target_label)

    # return the summation of the fake loss function and the real loss function (loss_value1)
    return loss + loss_value1


