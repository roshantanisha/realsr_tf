import tensorflow as tf
import numpy as np
from log import logging
from tqdm import tqdm


def initializer_conv4d(in_channels, out_channels, mapsize, mapsize2,
                       stddev_factor=1.0, mode='Glorot', graph=None):
    """Initialization in the style of Glorot 2010.
    stddev_factor should be 1.0 for linear activations, and 2.0 for ReLUs"""
    with graph.as_default():
        if mode == 'Glorot':
            stddev = np.sqrt(
                stddev_factor / (np.sqrt(in_channels * out_channels) * mapsize * mapsize * mapsize2 * mapsize2))
        else:
            stddev = 1.0

        init_value = tf.truncated_normal([mapsize, mapsize, mapsize2, mapsize2, in_channels, out_channels],
                            mean=0.0, stddev=stddev)
        return init_value


def conv4d(x, in_channels, out_channels, kernel_size_1=3, kernel_size_2=3,
           stride_1=1, stride_2=1, padding='SAME', stddev_factor=1.0, trainable=True, verbose=True, bias=True,
           activation='relu', name='convolution', graph=None):
    assert len(x.get_shape().as_list()) == 6 and \
           "Previous layer must be 6D: (batch, height, width, sview, tview, channels)"

    with graph.as_default():
        weight4d_init = initializer_conv4d(in_channels, out_channels, mapsize=kernel_size_1,
                                       mapsize2=kernel_size_2, stddev_factor=stddev_factor, graph=graph)

    with graph.as_default():
        filter4d = tf.get_variable(name='weight_' + name,
                                   initializer=weight4d_init,
                                   dtype=x.dtype,
                                   trainable=trainable)

    with graph.as_default():
        out = convolve4d(input=x, filter=filter4d,
                         strides=[1, stride_1, stride_1, stride_2, stride_2, 1],
                         padding=padding, bias=bias, activation=activation, graph=graph)

    if verbose:
        message = '|{0:-^72}|'.format(' Conv4d Layer: ' + str(out.get_shape()) + ' ')
        logging.info(message)
    return out


def convolve4d(input, filter,
               strides=[1, 1, 1, 1, 1, 1],
               padding='SAME',
               dilation_rate=None,
               stack_axis=None,
               stack_nested=False,
               bias=False,
               activation='relu', graph=None, name='name'
               ):
    '''
      Computes a convolution over 4 dimensions.
      Python generalization of tensorflow's conv3d with dilation.
      conv4d_stacked uses tensorflows conv3d and stacks results along
      stack_axis.

  Parameters
  ----------
  input : A Tensor.
          Shape [batch, x_dim, y_dim, z_dim, t_dim, in_channels]

  filter: A Tensor. Must have the same type as input.
          Shape [x_dim, y_dim, z_dim, t_dim, in_channels, out_channels].
          in_channels must match between input and filter

  strides: A list of ints that has length 6. 1-D tensor of length 6.
           The stride of the sliding window for each dimension of input.
           Must have strides[0] = strides[5] = 1.
  padding: A string from: "SAME", "VALID". The type of padding algorithm to use.

  dilation_rate: Optional. Sequence of 4 ints >= 1.
                 Specifies the filter upsampling/input downsampling rate.
                 Equivalent to dilation_rate in tensorflows tf.nn.convolution

  stack_axis: Int
            Axis along which the convolutions will be stacked.
            By default the axis with the lowest output dimensionality will be
            chosen. This is only an educated guess of the best choice!

  stack_nested: Bool
            If set to True, this will stack in a for loop seperately and afterwards
            combine the results. In most cases slower, but maybe less memory needed.

  Returns
  -------
          A Tensor. Has the same type as input.
    '''
    stack_axis = 3

    if dilation_rate != None:
        dilation_along_stack_axis = dilation_rate[stack_axis - 1]
    else:
        dilation_along_stack_axis = 1

    with graph.as_default():
        tensors_t = tf.unstack(input, axis=stack_axis)
        kernel_t = tf.unstack(filter, axis=stack_axis - 1)

    # noOfInChannels = input.get_shape().as_list()[-1]
    len_ts = filter.get_shape().as_list()[stack_axis - 1]
    size_of_t_dim = input.get_shape().as_list()[stack_axis]

    if len_ts % 2 == 1:
        # uneven filter size: same size to left and right
        filter_l = int(len_ts / 2)
        filter_r = int(len_ts / 2)
    else:
        # even filter size: one more to right
        filter_l = int(len_ts / 2) - 1
        filter_r = int(len_ts / 2)

    # The start index is important for strides and dilation
    # The strides start with the first element
    # that works and is VALID:
    start_index = 0
    if padding == 'VALID':
        for i in tqdm(range(size_of_t_dim)):
            if len(range(max(i - dilation_along_stack_axis * filter_l, 0),
                         min(i + dilation_along_stack_axis * filter_r + 1,
                             size_of_t_dim), dilation_along_stack_axis)
                   ) == len_ts:
                # we found the first index that doesn't need padding
                break
        start_index = i
        # print 'start_index', start_index

    # loop over all t_j in t
    result_t = []
    for i in range(start_index, size_of_t_dim, strides[stack_axis]):

        kernel_patch = []
        input_patch = []
        tensors_t_convoluted = []

        if padding == 'VALID':

            # Get indices t_s
            indices_t_s = range(max(i - dilation_along_stack_axis * filter_l, 0),
                                min(i + dilation_along_stack_axis * filter_r + 1, size_of_t_dim),
                                dilation_along_stack_axis)

            # check if Padding = 'VALID'
            if len(indices_t_s) == len_ts:

                # sum over all remaining index_t_i in indices_t_s
                for j, index_t_i in enumerate(indices_t_s):
                    if not stack_nested:
                        with graph.as_default():
                            kernel_patch.append(kernel_t[j])
                            input_patch.append(tensors_t[index_t_i])
                    else:
                        if dilation_rate != None:
                            with graph.as_default():
                                temp = tf.nn.convolution(input=tensors_t[index_t_i],
                                                         filter=kernel_t[j],
                                                         strides=strides[1:stack_axis + 1] + strides[stack_axis:5],
                                                         padding=padding,
                                                         dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                    stack_axis:])
                            if bias:
                                with graph.as_default():
                                    temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                            if activation != 'linear':
                                with graph.as_default():
                                    temp = getattr(tf.nn, activation)(temp)
                            tensors_t_convoluted.append(temp)
                        else:
                            with graph.as_default():
                                temp = tf.nn.conv3d(input=tensors_t[index_t_i],
                                                    filter=kernel_t[j],
                                                    strides=strides[:stack_axis] + strides[stack_axis + 1:],
                                                    padding=padding)
                            if bias:
                                with graph.as_default():
                                    temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                            if activation != 'linear':
                                with graph.as_default():
                                    temp = getattr(tf.nn, activation)(temp)
                            tensors_t_convoluted.append(temp)
                if stack_nested:
                    with graph.as_default():
                        sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                    # put together
                    result_t.append(sum_tensors_t_s)

        elif padding == 'SAME':

            # Get indices t_s
            indices_t_s = range(i - dilation_along_stack_axis * filter_l,
                                (i + 1) + dilation_along_stack_axis * filter_r,
                                dilation_along_stack_axis)

            for kernel_j, j in enumerate(indices_t_s):
                # we can just leave out the invalid t coordinates
                # since they will be padded with 0's and therfore
                # don't contribute to the sum

                if 0 <= j < size_of_t_dim:
                    if not stack_nested:
                        with graph.as_default():
                            kernel_patch.append(kernel_t[kernel_j])
                            input_patch.append(tensors_t[j])
                    else:
                        if dilation_rate != None:
                            with graph.as_default():
                                temp = tf.nn.convolution(input=tensors_t[j],
                                                         filter=kernel_t[kernel_j],
                                                         strides=strides[1:stack_axis + 1] + strides[stack_axis:5],
                                                         padding=padding,
                                                         dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                        stack_axis:])
                            if bias:
                                with graph.as_default():
                                    temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                            if activation != 'linear':
                                with graph.as_default():
                                    temp = getattr(tf.nn, activation)(temp)
                            tensors_t_convoluted.append(temp)
                        else:
                            with graph.as_default():
                                temp = tf.nn.conv3d(input=tensors_t[j],
                                                    filter=kernel_t[kernel_j],
                                                    strides=strides[:stack_axis] + strides[stack_axis + 1:],
                                                    padding=padding, bias=bias)
                            if bias:
                                with graph.as_default():
                                    temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                            if activation != 'linear':
                                with graph.as_default():
                                    temp = getattr(tf.nn, activation)(temp)
                            tensors_t_convoluted.append(temp)
            if stack_nested:
                with graph.as_default():
                    sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                # put together
                result_t.append(sum_tensors_t_s)

        if not stack_nested:
            if kernel_patch:
                with graph.as_default():
                    kernel_patch = tf.concat(kernel_patch, axis=3)
                    input_patch = tf.concat(input_patch, axis=4)
                    kernel_patch1 = kernel_patch
                    # kernel_patch1 = tf.transpose(kernel_patch, (0, 1, 2, 4, 3))
                if dilation_rate != None:
                    with graph.as_default():
                        result_patch = tf.nn.convolution(input=input_patch,
                                                         filter=kernel_patch1,
                                                         strides=strides[1:stack_axis] + strides[stack_axis + 1:5],
                                                         padding=padding,
                                                         dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                        stack_axis:])
                    # result_patch = tf.transpose(result_patch, (0, 1, 2, 4, 3))
                    if bias:
                        with graph.as_default():
                            zeros1 = tf.zeros(shape=(result_patch.shape.as_list()[-1]))
                            result_patch = tf.nn.bias_add(result_patch, zeros1)
                    if activation != 'linear':
                        with graph.as_default():
                            result_patch = getattr(tf.nn, activation)(result_patch)
                else:
                    with graph.as_default():
                        result_patch = tf.nn.conv3d(input=input_patch,
                                                    filters=kernel_patch1,
                                                    # output_shape=input_patch.shape,
                                                    strides=strides[:stack_axis] + strides[stack_axis + 1:],
                                                    padding=padding)
                        # result_patch = tf.transpose(result_patch, (0, 1, 2, 4, 3))
                    if bias:
                        with graph.as_default():
                            zeros2 = tf.zeros(shape=(result_patch.shape.as_list()[-1]))
                            result_patch = tf.nn.bias_add(result_patch, zeros2)
                    if activation != 'linear':
                        with graph.as_default():
                            result_patch = getattr(tf.nn, activation)(result_patch)
                result_t.append(result_patch)

    # stack together
    with graph.as_default():
        return_value = tf.stack(result_t, axis=stack_axis, name='4dconvolution_' + name)
    return return_value


def conv4d_transpose(x, in_channels, out_channels, kernel_size_1=3, kernel_size_2=3,
           stride_1=1, stride_2=1, padding='SAME', stddev_factor=1.0, trainable=True, verbose=True, bias=True,
           activation='relu', name='convolution', graph=None):
    assert len(x.get_shape().as_list()) == 6 and \
           "Previous layer must be 6D: (batch, height, width, sview, tview, channels)"

    with graph.as_default():
        weight4d_init = initializer_conv4d(out_channels, in_channels, mapsize=kernel_size_1,
                                       mapsize2=kernel_size_2, stddev_factor=stddev_factor, graph=graph)

    with graph.as_default():
        filter4d = tf.get_variable(name='weight_' + name,
                                   initializer=weight4d_init,
                                   dtype=x.dtype,
                                   trainable=trainable)

    with graph.as_default():
        out = convolve4d_transpose(input=x, filter=filter4d,
                         strides=[1, stride_1, stride_1, stride_2, stride_2, 1],
                         padding=padding, bias=bias, activation=activation, graph=graph)

    if verbose:
        message = '|{0:-^72}|'.format(' Conv4d Layer: ' + str(out.get_shape()) + ' ')
        logging.info(message)
    return out


# def transpose_conv(i, k, strides):
def convolve4d_transpose(input, filter,
                         strides=[1, 1, 1, 1, 1, 1],
                         padding='SAME',
                         dilation_rate=None,
                         stack_axis=None,
                         stack_nested=False,
                         bias=True,
                         activation='relu',
                         graph=None
                         ):
    '''
      Computes a convolution over 4 dimensions.
      Python generalization of tensorflow's conv3d with dilation.
      conv4d_stacked uses tensorflows conv3d and stacks results along
      stack_axis.

  Parameters
  ----------
  input : A Tensor.
          Shape [batch, x_dim, y_dim, z_dim, t_dim, in_channels]

  filter: A Tensor. Must have the same type as input.
          Shape [x_dim, y_dim, z_dim, t_dim, in_channels, out_channels].
          in_channels must match between input and filter

  strides: A list of ints that has length 6. 1-D tensor of length 6.
           The stride of the sliding window for each dimension of input.
           Must have strides[0] = strides[5] = 1.
  padding: A string from: "SAME", "VALID". The type of padding algorithm to use.

  dilation_rate: Optional. Sequence of 4 ints >= 1.
                 Specifies the filter upsampling/input downsampling rate.
                 Equivalent to dilation_rate in tensorflows tf.nn.convolution

  stack_axis: Int
            Axis along which the convolutions will be stacked.
            By default the axis with the lowest output dimensionality will be
            chosen. This is only an educated guess of the best choice!

  stack_nested: Bool
            If set to True, this will stack in a for loop seperately and afterwards
            combine the results. In most cases slower, but maybe less memory needed.

  Returns
  -------
          A Tensor. Has the same type as input.
    '''

    stack_axis = 3
    output_shape = [input.shape[i] + filter.shape[i - 1] - strides[i - 1] for i in range(1, len(input.shape))]
    output_shape.append(filter.shape[-1])

    if dilation_rate != None:
        dilation_along_stack_axis = dilation_rate[stack_axis - 1]
    else:
        dilation_along_stack_axis = 1

    with graph.as_default():
        tensors_t = tf.unstack(input, axis=stack_axis)
        kernel_t = tf.unstack(filter, axis=stack_axis - 1)

    noOfInChannels = input.get_shape().as_list()[-1]
    len_ts = filter.get_shape().as_list()[stack_axis - 1]
    size_of_t_dim = input.get_shape().as_list()[stack_axis]

    if len_ts % 2 == 1:
        # uneven filter size: same size to left and right
        filter_l = int(len_ts / 2)
        filter_r = int(len_ts / 2)
    else:
        # even filter size: one more to right
        filter_l = int(len_ts / 2) - 1
        filter_r = int(len_ts / 2)

    # The start index is important for strides and dilation
    # The strides start with the first element
    # that works and is VALID:
    start_index = 0
    if padding == 'VALID':
        for i in tqdm(range(size_of_t_dim)):
            if len(range(max(i - dilation_along_stack_axis * filter_l, 0),
                         min(i + dilation_along_stack_axis * filter_r + 1,
                             size_of_t_dim), dilation_along_stack_axis)
                   ) == len_ts:
                # we found the first index that doesn't need padding
                break
        start_index = i
        # print 'start_index', start_index

    # loop over all t_j in t
    result_t = []
    for i in range(start_index, size_of_t_dim, strides[stack_axis]):

        kernel_patch = []
        input_patch = []
        tensors_t_convoluted = []

        if padding == 'VALID':

            # Get indices t_s
            indices_t_s = range(max(i - dilation_along_stack_axis * filter_l, 0),
                                min(i + dilation_along_stack_axis * filter_r + 1, size_of_t_dim),
                                dilation_along_stack_axis)

            # check if Padding = 'VALID'
            if len(indices_t_s) != len_ts:

                # sum over all remaining index_t_i in indices_t_s
                for j, index_t_i in enumerate(indices_t_s):
                    if not stack_nested:
                        with graph.as_default():
                            kernel_patch.append(kernel_t[j])
                            input_patch.append(tensors_t[index_t_i])
                    else:
                        if dilation_rate != None:
                            with graph.as_default():
                                temp = tf.nn.conv_transpose(input=tensors_t[index_t_i],
                                                     filters=kernel_t[j],
                                                     strides=strides[
                                                             1:stack_axis + 1] + strides[
                                                                                 stack_axis:5],
                                                     padding=padding,
                                                     output_shape=output_shape,
                                                     dilation_rate=dilation_rate[
                                                                   :stack_axis - 1] + dilation_rate[
                                                                                      stack_axis:])
                                if bias:
                                    with graph.as_default():
                                        temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                                if activation != 'linear':
                                    with graph.as_default():
                                        temp = getattr(tf.nn, activation)(temp)

                                tensors_t_convoluted.append(temp)
                        else:
                            with graph.as_default():
                                temp = tf.nn.conv3d_transpose(input=tensors_t[index_t_i],
                                                                               filters=kernel_t[j],
                                                                               output_shape=output_shape,
                                                                               strides=strides[:stack_axis] + strides[
                                                                                                              stack_axis + 1:],
                                                                               padding=padding)
                                if bias:
                                    with graph.as_default():
                                        temp = tf.nn.bias_add(temp, tf.zeros(shape=temp.shape.as_list()[-1]))
                                if activation != 'linear':
                                    with graph.as_default():
                                        temp = getattr(tf.nn, activation)(temp)
                                tensors_t_convoluted.append(temp)
                if stack_nested:
                    with graph.as_default():
                        sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                        # put together
                        result_t.append(sum_tensors_t_s)

        elif padding == 'SAME':

            # Get indices t_s
            indices_t_s = range(i - dilation_along_stack_axis * filter_l,
                                (i + 1) + dilation_along_stack_axis * filter_r,
                                dilation_along_stack_axis)

            for kernel_j, j in enumerate(indices_t_s):
                # we can just leave out the invalid t coordinates
                # since they will be padded with 0's and therfore
                # don't contribute to the sum

                if 0 <= j < size_of_t_dim:
                    if not stack_nested:
                        with graph.as_default():
                            kernel_patch.append(kernel_t[kernel_j])
                            input_patch.append(tensors_t[j])
                    else:
                        if dilation_rate != None:
                            with graph.as_default():
                                temp = tf.nn.conv_transpose(input=tensors_t[j],
                                                                             filter=kernel_t[kernel_j],
                                                                             strides=strides[
                                                                                     1:stack_axis + 1] + strides[
                                                                                                         stack_axis:5],
                                                                             padding=padding,
                                                                             output_shape=output_shape,
                                                                             dilation_rate=dilation_rate[
                                                                                           :stack_axis - 1] + dilation_rate[
                                                                                                              stack_axis:])
                                if bias:
                                    with graph.as_default():
                                        temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                                if activation != 'linear':
                                    with graph.as_default():
                                        temp = getattr(tf.nn, activation)(temp)
                                tensors_t_convoluted.append(temp)
                        else:
                            with graph.as_default():
                                temp = tf.nn.conv3d_transpose(input=tensors_t[j],
                                                                                   filter=kernel_t[kernel_j],
                                                                                   output_shape=output_shape,
                                                                                   strides=strides[
                                                                                           :stack_axis] + strides[
                                                                                                          stack_axis + 1:],
                                                                                   padding=padding)
                                if bias:
                                    with graph.as_default():
                                        temp = tf.nn.bias_add(temp, tf.zeros(shape=(temp.shape.as_list()[-1])))
                                if activation != 'linear':
                                    with graph.as_default():
                                        temp = getattr(tf.nn, activation)(temp)
                                tensors_t_convoluted.append(temp)

            if stack_nested:
                with graph.as_default():
                    sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                # put together
                result_t.append(sum_tensors_t_s)

        if not stack_nested:
            if kernel_patch:
                with graph.as_default():
                    kernel_patch = tf.concat(kernel_patch, axis=stack_axis + 1)
                    input_patch = tf.concat(input_patch, axis=stack_axis + 1)
                if dilation_rate != None:
                    with graph.as_default():
                        result_patch = tf.nn.conv_transpose(input=input_patch,
                                                            filter=kernel_patch,
                                                            output_shape=output_shape,
                                                            strides=strides[1:stack_axis] + strides[stack_axis + 1:5],
                                                            padding=padding,
                                                            dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                           stack_axis:])
                        if bias:
                            with graph.as_default():
                                zeros1 = tf.zeros(shape=(result_patch.shape.as_list()[-1]))
                                result_patch = tf.nn.bias_add(result_patch, zeros1)
                        if activation != 'linear':
                            with graph.as_default():
                                result_patch = getattr(tf.nn, activation)(result_patch)
                else:
                    # print(input_patch.shape, kernel_patch.shape)
                    assert input_patch.shape[-1] == kernel_patch.shape[-1]
                    if padding == 'VALID':
                        # print(input_patch.shape, kernel_patch.shape)
                        output_shape = [(input_patch.shape[i + 1] - 1) * strides[i] + kernel_patch.shape[i] for i in
                                        range(3)]
                        output_shape.append(kernel_patch.shape[-2])
                        output_shape.insert(0, input_patch.shape[0])
                        # print('now = ', output_shape)
                    if padding == 'SAME':
                        output_shape = input_patch.shape.as_list()
                        # print(output_shape)
                        output_shape[-1] = kernel_patch.shape[-2]

                    with graph.as_default():
                        result_patch = tf.nn.conv3d_transpose(value=input_patch,
                                                              filters=kernel_patch,
                                                              output_shape=output_shape,
                                                              strides=strides[:stack_axis + 1] + strides[stack_axis + 2:],
                                                              padding=padding)
                        if bias:
                            with graph.as_default():
                                result_patch = tf.nn.bias_add(result_patch, tf.zeros(shape=(result_patch.shape.as_list()[-1])))
                        if activation != 'linear':
                            with graph.as_default():
                                result_patch = getattr(tf.nn, activation)(result_patch)

                result_t.append(result_patch)

    # stack together
    with graph.as_default():
        return_value = tf.stack(result_t, axis=stack_axis)
    return return_value


def convolve3d_transpose(input, filter,
                         strides=[1, 1, 1, 1, 1],
                         padding='SAME',
                         dilation_rate=None,
                         stack_axis=None,
                         stack_nested=False,
                         ):
    '''
      Computes a convolution over 4 dimensions.
      Python generalization of tensorflow's conv3d with dilation.
      conv4d_stacked uses tensorflows conv3d and stacks results along
      stack_axis.

  Parameters
  ----------
  input : A Tensor.
          Shape [batch, x_dim, y_dim, z_dim, t_dim, in_channels]

  filter: A Tensor. Must have the same type as input.
          Shape [x_dim, y_dim, z_dim, t_dim, in_channels, out_channels].
          in_channels must match between input and filter

  strides: A list of ints that has length 6. 1-D tensor of length 6.
           The stride of the sliding window for each dimension of input.
           Must have strides[0] = strides[5] = 1.
  padding: A string from: "SAME", "VALID". The type of padding algorithm to use.

  dilation_rate: Optional. Sequence of 4 ints >= 1.
                 Specifies the filter upsampling/input downsampling rate.
                 Equivalent to dilation_rate in tensorflows tf.nn.convolution

  stack_axis: Int
            Axis along which the convolutions will be stacked.
            By default the axis with the lowest output dimensionality will be
            chosen. This is only an educated guess of the best choice!

  stack_nested: Bool
            If set to True, this will stack in a for loop seperately and afterwards
            combine the results. In most cases slower, but maybe less memory needed.

  Returns
  -------
          A Tensor. Has the same type as input.
    '''
    stack_axis = 2
    output_shape = [input.shape[i] + filter.shape[i - 1] - strides[i - 1] for i in range(1, len(input.shape))]
    output_shape.append(filter.shape[-1])

    if dilation_rate != None:
        dilation_along_stack_axis = dilation_rate[stack_axis - 1]
    else:
        dilation_along_stack_axis = 1

    tensors_t = tf.unstack(input, axis=stack_axis)
    kernel_t = tf.unstack(filter, axis=stack_axis - 1)

    print(input.shape, filter.shape)

    print(len(tensors_t), tensors_t[0].shape)
    print(len(kernel_t), kernel_t[0].shape)

    noOfInChannels = input.get_shape().as_list()[-1]
    len_ts = filter.get_shape().as_list()[stack_axis - 1]
    size_of_t_dim = input.get_shape().as_list()[stack_axis]

    k_prime = (size_of_t_dim + 2 * (size_of_t_dim - 1) - len_ts - (len_ts - 1) * (dilation_along_stack_axis - 1)) // \
              strides[stack_axis] + 1

    print('11111', len_ts, size_of_t_dim, k_prime)

    if len_ts % 2 == 1:
        # uneven filter size: same size to left and right
        filter_l = int(len_ts / 2)
        filter_r = int(len_ts / 2)
    else:
        # even filter size: one more to right
        filter_l = int(len_ts / 2) - 1
        filter_r = int(len_ts / 2)

    print(filter_r, filter_l)

    # The start index is important for strides and dilation
    # The strides start with the first element
    # that works and is VALID:
    start_index = 0
    if padding == 'VALID':
        for i in tqdm(range(k_prime)):
            print(i - dilation_along_stack_axis * filter_l, 0, '222')
            print(i + dilation_along_stack_axis * filter_r + 1,
                  size_of_t_dim, '111')
            if len(range(max(i - dilation_along_stack_axis * filter_l, 0),
                         min(i + dilation_along_stack_axis * filter_r + 1,
                             size_of_t_dim), dilation_along_stack_axis)
                   ) == len_ts:
                # we found the first index that doesn't need padding
                break
        start_index = i
        # print 'start_index', start_index

    # loop over all t_j in t
    print('start_index = ', start_index)
    result_t = []
    true = 0
    for i in range(start_index, k_prime, strides[stack_axis]):

        kernel_patch = []
        input_patch = []
        tensors_t_convoluted = []

        if padding == 'VALID':

            # Get indices t_s
            print(range(max(i + dilation_along_stack_axis * filter_l, 0),
                        min(i - dilation_along_stack_axis * filter_r + 1, k_prime),
                        dilation_along_stack_axis))
            indices_t_s = range(max(i - dilation_along_stack_axis * filter_l, 0),
                                min(i + dilation_along_stack_axis * filter_r + 1, k_prime),
                                dilation_along_stack_axis)

            print(len(indices_t_s), 'len_indices_t_s', len_ts)

            # check if Padding = 'VALID'
            # print('ere', len(indices_t_s))
            # if len(indices_t_s) == len_ts:
            #     true += 1
            #     print(true, 'true')
            #
            #     # sum over all remaining index_t_i in indices_t_s
            for j, index_t_i in enumerate(indices_t_s):
                print('j', j)
                if not stack_nested:
                    kernel_patch.append(kernel_t[j])
                    input_patch.append(tensors_t[index_t_i])
                else:
                    if dilation_rate != None:
                        tensors_t_convoluted.append(tf.nn.conv_transpose(input=tensors_t[index_t_i],
                                                                         filters=kernel_t[j],
                                                                         strides=strides[
                                                                                 1:stack_axis + 1] + strides[
                                                                                                     stack_axis:5],
                                                                         padding=padding,
                                                                         output_shape=output_shape,
                                                                         dilation_rate=dilation_rate[
                                                                                       :stack_axis - 1] + dilation_rate[
                                                                                                          stack_axis:])
                                                    )
                    else:
                        tensors_t_convoluted.append(tf.nn.conv2d_transpose(input=tensors_t[index_t_i],
                                                                           filters=kernel_t[j],
                                                                           output_shape=output_shape,
                                                                           strides=strides[:stack_axis] + strides[
                                                                                                          stack_axis + 1:],
                                                                           padding=padding)
                                                    )
            if stack_nested:
                sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                # put together
                result_t.append(sum_tensors_t_s)

        elif padding == 'SAME':

            # Get indices t_s
            indices_t_s = range(i - dilation_along_stack_axis * filter_l,
                                (i + 1) + dilation_along_stack_axis * filter_r,
                                dilation_along_stack_axis)

            for kernel_j, j in enumerate(indices_t_s):
                # we can just leave out the invalid t coordinates
                # since they will be padded with 0's and therfore
                # don't contribute to the sum

                print('-->', j, size_of_t_dim)
                if 0 <= j < size_of_t_dim:
                    if not stack_nested:
                        kernel_patch.append(kernel_t[kernel_j])
                        input_patch.append(tensors_t[j])
                    else:
                        if dilation_rate != None:
                            tensors_t_convoluted.append(tf.nn.conv_transpose(input=tensors_t[j],
                                                                             filter=kernel_t[kernel_j],
                                                                             strides=strides[
                                                                                     1:stack_axis + 1] + strides[
                                                                                                         stack_axis:5],
                                                                             padding=padding,
                                                                             output_shape=output_shape,
                                                                             dilation_rate=dilation_rate[
                                                                                           :stack_axis - 1] + dilation_rate[
                                                                                                              stack_axis:])
                                                        )
                        else:
                            tensors_t_convoluted.append(tf.nn.conv2d_transpose(input=tensors_t[j],
                                                                               filter=kernel_t[kernel_j],
                                                                               output_shape=output_shape,
                                                                               strides=strides[
                                                                                       :stack_axis] + strides[
                                                                                                      stack_axis + 1:],
                                                                               padding=padding)
                                                        )
            if stack_nested:
                sum_tensors_t_s = tf.add_n(tensors_t_convoluted)
                # put together
                result_t.append(sum_tensors_t_s)

        if not stack_nested:
            print('....', len(kernel_patch))
            if kernel_patch:
                print(kernel_patch[0].shape, len(kernel_patch))
                kernel_patch = tf.concat(kernel_patch, axis=stack_axis)
                print('k', kernel_patch.shape)
                print(input_patch[0].shape, len(input_patch))
                input_patch = tf.concat(input_patch, axis=stack_axis)
                print('i', input_patch.shape)
                if dilation_rate != None:
                    print('dilr')
                    result_patch = tf.nn.conv_transpose(input=input_patch,
                                                        filter=kernel_patch,
                                                        output_shape=output_shape,
                                                        strides=strides[1:stack_axis] + strides[stack_axis + 1:5],
                                                        padding=padding,
                                                        dilation_rate=dilation_rate[:stack_axis - 1] + dilation_rate[
                                                                                                       stack_axis:])
                else:
                    assert input_patch.shape[-1] == kernel_patch.shape[-1]
                    if padding == 'VALID':
                        # print(input_patch.shape, kernel_patch.shape)
                        output_shape = [(input_patch.shape[i + 1] - 1) * strides[i] + kernel_patch.shape[i] for i in
                                        range(1)]
                        output_shape.append(kernel_patch.shape[-2])
                        output_shape.insert(0, input_patch.shape[0])
                        # print('now = ', output_shape)
                    if padding == 'SAME':
                        output_shape = input_patch.shape.as_list()
                        # print(output_shape)
                        output_shape[-1] = kernel_patch.shape[-2]

                    print('before con', input_patch.shape, kernel_patch.shape, output_shape,
                          strides[:stack_axis + 1] + strides[stack_axis + 2:])
                    result_patch = tf.nn.conv1d_transpose(input=input_patch,
                                                          filters=kernel_patch,
                                                          output_shape=output_shape,
                                                          strides=strides[:stack_axis + 1] + strides[stack_axis + 2:],
                                                          padding=padding)
                result_t.append(result_patch)
                print(result_patch.shape, len(result_t), 'result patch')

    # stack together
    print(len(result_t))
    return tf.stack(result_t, axis=stack_axis)


def test_conv3d():
    a = tf.zeros(shape=(1, 2, 2, 2, 3))
    kernel = tf.zeros(shape=(1, 1, 1, 9, 3))
    output_shape = [1, 2, 2, 2, 9]

    output = tf.nn.conv3d_transpose(input=a,
                                    filters=kernel,
                                    output_shape=output_shape, strides=[1, 1, 1, 1, 1])

    print('->', output.shape)
